"""
HeuristicPlacer is subproblem placement strategy that places
a subproblem to cover a conflict by first finding all possible
subproblems, and choosing the best one according to some heuristics
"""

import random
import numpy as np
import sys

from guided_mrmp.conflict_resolvers.subproblems import Subproblem

class HeuristicPlacer:
    def __init__(self, grid, conflict, all_robots, all_conflicts):
        """
        inputs:
            - grid (np array): map of the environment (obstacle map)
            - conflict (Conflict): the conflict the subproblem must cover
            - all_conflicts (list): All of the conflicts, so that we can maximize the number of conflicts resolved
            - all_robots (list): all of the robots
            - type (string): the type of subproblem
        """
        self.map = grid
        self.conflict = conflict
        self.conflicts = all_conflicts
        self.all_robots = all_robots
        # self.type = type
        self.all_robots_involved_in_subproblem = []

        # The temporary starts/goals of the subpproblem. 
        # These are still in the original environment's coordinates
        self.temp_starts = None
        self.temp_goals = None

        # Location of the subproblem in the original environment's coordinates
        self.top_left = None
        self.bottom_right = None

        # get the indices of the robots that are actually in conflict
        self.robots_in_conflict = [r.label for r in conflict]

        # get the smallest/biggest x/y value of the robots in conflict
        # this gives us the area that our subproblem needs to cover
        self.row_range, self.col_range, self.leftmost_robot, self.rightmost_robot, \
            self.topmost_robot, self.bottommost_robot = self.get_range_of_subproblem()
        
        self.subproblem_layout = None
        self.score = None
        self.waiting_score = None
        self.conflicts_covered = None
         
    def get_range_of_subproblem(self):
        """
        Find the smallest and largest x/y values. 
        This will tell us what range our subproblem needs to cover
        """
        smallest_row = None
        smallest_col = None
        largest_row = None
        largest_col = None
        leftmost_robot = None
        rightmost_robot = None
        topmost_robot = None
        bottommost_robot = None
        for robot_idx in self.robots_in_conflict:
            robot_position =  self.all_robots[robot_idx].current_position
            if smallest_row is None or smallest_col is None:
                smallest_row = robot_position[0]
                smallest_col = robot_position[1]
                topmost_robot = robot_idx
                leftmost_robot = robot_idx

            else:
                if robot_position[0] < smallest_row:
                    smallest_row = robot_position[0]
                    topmost_robot = robot_idx
                if robot_position[1] < smallest_col:
                    smallest_col = robot_position[1]
                    leftmost_robot = robot_idx
            
            if largest_row is None or largest_col is None:
                largest_row = robot_position[0]
                largest_col = robot_position[1]
                rightmost_robot = robot_idx
                bottommost_robot = robot_idx

            else:
                if robot_position[0] > largest_row:
                    largest_row = robot_position[0]
                    bottommost_robot = robot_idx
                if robot_position[1] > largest_col:
                    largest_col = robot_position[1]
                    rightmost_robot = robot_idx
        row_range = abs(largest_row - smallest_row)
        col_range = abs(largest_col - smallest_col)

        return row_range, col_range, leftmost_robot, rightmost_robot, topmost_robot, bottommost_robot
  
    def find_best_of_type(self, S, find_best, type):
        """
        Find the best subproblem that contains the conflict completely. 
        "best" in this case means covering the most conflicts
        inputs: 
            S- list of subproblems that cannot be overlapped with
        outputs: 
            (top_left,bottom_right) - location of best subproblem of this type
        """
        
        # We start by finding all possible valid subproblems (of this type) 
        # and then choosing the best one at the end
        # initialize the list of all found subproblems to be empty.
        possible_subproblems = []

        if type == 23: 
            row_val = 2
            col_val = 3
        elif type == 33: 
            row_val = 3
            col_val = 3
        else:
            row_val = 2
            col_val = 5 
        
        if self.row_range <= row_val and self.col_range <= col_val:
            # look for a nxm
            row_shift = row_val-1
            col_shift = col_val-1
            left_robot = self.all_robots[self.leftmost_robot].current_position
            top_robot = self.all_robots[self.topmost_robot].current_position

            for r in range(row_shift+1):
                for c in range(col_shift+1):

                    top_left = (top_robot[0]-r, left_robot[1]-c)
                    bottom_right = (top_left[0]+row_shift, top_left[1]+col_shift)

                    s = Subproblem(type, top_left, bottom_right)

                    if s.is_valid(top_left, bottom_right,S, self.all_robots, self.map, self.conflict):
                        
                        conflicts, score, num_waiting = self.score_subproblem(top_left, bottom_right)
                        if not find_best: 
                            return ((top_left, bottom_right), score, conflicts,num_waiting)
                        possible_subproblems.append(((top_left, bottom_right), score, conflicts, num_waiting))

                    right_robot = self.all_robots[self.rightmost_robot].current_position
                    bottom_robot = self.all_robots[self.bottommost_robot].current_position
                    bottom_right = (bottom_robot[0]+r, right_robot[1]+c)
                    top_left = (bottom_right[0]-row_shift, bottom_right[1]-col_shift)

                    s = Subproblem(type, top_left, bottom_right)

                    if s.is_valid(top_left, bottom_right,S, self.all_robots, self.map, self.conflict):
                        
                        conflicts, score, num_waiting = self.score_subproblem(top_left, bottom_right)
                        if not find_best: 
                            return ((top_left, bottom_right), score, conflicts,num_waiting)
                        possible_subproblems.append(((top_left, bottom_right), score, conflicts, num_waiting))

        if row_val != col_val:
            if self.row_range <= col_val and self.col_range <= row_val:
                # look for a mxn
                row_shift = col_val-1
                col_shift = row_val-1
                left_robot = self.all_robots[self.leftmost_robot].current_position
                top_robot = self.all_robots[self.topmost_robot].current_position
                for r in range(row_shift+1):
                    for c in range(col_shift+1):
                        top_left = (left_robot[0]-r, top_robot[1]-c)
                        bottom_right = (top_left[0]+row_shift, top_left[1]+col_shift)

                        s = Subproblem(type, top_left, bottom_right)

                        if s.is_valid(top_left, bottom_right,S, self.all_robots, self.map, self.conflict):
                            
                            conflicts, score, num_waiting = self.score_subproblem(top_left, bottom_right)
                            if not find_best: 
                                return ((top_left, bottom_right), score, conflicts, num_waiting)
                            possible_subproblems.append(((top_left, bottom_right), score, conflicts, num_waiting))

                        right_robot = self.all_robots[self.rightmost_robot].current_position
                        bottom_robot = self.all_robots[self.bottommost_robot].current_position
                        bottom_right = (right_robot[0]+r, bottom_robot[1]+c)
                        top_left = (bottom_right[0]-row_shift, bottom_right[1]-col_shift)

                        s = Subproblem(type, top_left, bottom_right)
                        if s.is_valid(top_left, bottom_right,S, self.all_robots, self.map, self.conflict):
                            
                            conflicts, score, num_waiting = self.score_subproblem(top_left, bottom_right)
                            if not find_best: 
                                return ((top_left, bottom_right), score, conflicts, num_waiting)
                            possible_subproblems.append(((top_left, bottom_right), score, conflicts, num_waiting))

        # if there are no valid subproblems, return None. 
        # This conflict will be handled by waiting instead of a subproblem
        if possible_subproblems == []: 
            return None 
        
        # otherwise, return the best of what we have    
        best = max(possible_subproblems, key=lambda x:x[1])

        most_conflicts_covered = best[1]
        # print(f"best score = {best_score}")
        cands_with_best_score = []
        for cand in possible_subproblems:
            if cand[1] == most_conflicts_covered:
                cands_with_best_score.append(cand)

        best = min(cands_with_best_score, key=lambda x:x[3])
        return best

    def score_subproblem(self, s):
        """
        Score a subproblem. It's score is the number of conflicts that it covers
        """
        max_x = s.bottom_right[0]
        min_x = s.top_left[0]
        min_y = s.top_left[1]
        max_y = s.bottom_right[1]
        
        # part of the score will be the number of conflicts
        conflicts = []
        for c in self.conflicts:
            if s.contains_conflict(min_x, max_x, min_y, max_y, c, self.all_robots):
                conflicts.append(c)

        num_conflicts = len(conflicts)

        # the other part of the score will be minimizing the number of waiting robots
        # get the number robots whose next desired cell is in this subproblem
        num_waiting = 0
        for r in self.all_robots:
            next = r.get_next_node()
            current = r.get_current_position()

            if (next[0] <= max_x and next[0] >= min_x and next[1] <= max_y and next[1] >= min_y) \
                and not ( current[0] <= max_x and current[0] >= min_x and current[1] <= max_y and current[1] >= min_y):

                num_waiting += 1

        return conflicts, num_conflicts, num_waiting

    def find_best_subproblem(self, S, types):
        """
        Find the best subproblem out of all the types we have
        """

        candidate_subprobs = []
        for s_type in types:
            best = self.find_best_of_type(S, True, s_type)
            if best is not None:
                candidate_subprobs.append([best, best.score, best.waiting_score, best.size])

        if len(candidate_subprobs) == 0: return None

        best = max(candidate_subprobs, key=lambda x:x[1])

        most_conflicts_covered = best[1]
        # print(f"best score = {best_score}")
        cands_with_most_conflicts_covered = []
        for cand in candidate_subprobs:
            if cand[1] == most_conflicts_covered:
                cands_with_most_conflicts_covered.append(cand)

        best_waiting = min(cands_with_most_conflicts_covered, key=lambda x:x[2])
        best_waiting_score = best_waiting[2]
        # print(f"best score = {best_score}")
        cands_with_most_conflicts_covered_and_best_waiting = []
        for cand in candidate_subprobs:
            if cand[1] == most_conflicts_covered and cand[2] == best_waiting:
                cands_with_most_conflicts_covered_and_best_waiting.append(cand)

        if cands_with_most_conflicts_covered_and_best_waiting == []:
            final_candidates = cands_with_most_conflicts_covered
        else: final_candidates = cands_with_most_conflicts_covered_and_best_waiting 

        best = min(final_candidates, key=lambda x:x[3])

        best_subproblem = best[0]

        return best_subproblem