import numpy as np
import random 

class Subproblem:
    def __init__(self, type, top_left, bottom_right):
        """
        inputs:
            - type (string): the type of subproblem (23, 33, or 25)
            - top_left (tuple): top_left location on grid
            - bottom_right (tuple)
        """
        self.type = type

        # The temporary starts/goals of the subpproblem. 
        # These are still in the original environment's coordinates
        self.temp_starts = None
        self.temp_goals = None

        # Location of the subproblem in the original environment's coordinates
        self.top_left = top_left
        self.bottom_right = bottom_right

        self.subproblem_layout = None

    def get_robots_involved_in_subproblem(self, top_left, bottom_right, all_robots):
        """
        Get all of the robots from all_robots that should be included in the subproblem

        inputs:
            top_left (node (r,c))- The top left node of the subproblem
            bottom_right (node (r,c))- The top left node of the subproblem
        outputs:
            all_robots (list of robots) - the robots involved in the subproblem
        """
        all_robots = []
        
        max_x = bottom_right[0]
        min_x = top_left[0]
        min_y = top_left[1]
        max_y = bottom_right[1]
        
        # any robot whose current and next node are in the subproblem are considered 
        # involved in the subproblem
        for r in all_robots:
            pos_curr = r.get_current_position() 
            pos_next = r.get_next_node() 
            x = pos_curr[0]
            y = pos_curr[1]
            x_next = pos_next[0]
            y_next = pos_next[1]

            if x <= max_x and x >= min_x and y <= max_y and y >= min_y and \
                x_next <= max_x and x_next >= min_x and y_next <= max_y and y_next >= min_y:
                all_robots.append(r)

        return all_robots
    
    def contains_conflict(self, min_x, max_x, min_y, max_y, conflict, all_robots):
        """
        Check if a given conflict is contained in the subproblem with the input maxes and mins
        """
        robots_in_conflict = conflict.get_robot_idxs()
        for r in robots_in_conflict:
            pos = all_robots[r].get_current_position()
            x = pos[0]
            y = pos[1]

            if x > max_x or x < min_x or y > max_y or y < min_y:
                return False
            
        return True

    def rotate(self):
        """
        Rotate the subproblem 180 degrees and then update starts and goals
        """
        subproblem = self.subproblem_layout
        subproblem = np.rot90(subproblem)
        # subproblem = np.rot90(subproblem)
        self.subproblem_layout = subproblem

        self.update_local_starts_and_goals()

    def flip(self):
        """
        Flip the subproblem over the horizontal axis and then update starts and goals
        """
        subproblem = self.subproblem_layout
        subproblem = np.flip(subproblem,1)
        self.subproblem_layout = subproblem
        self.update_local_starts_and_goals()
   
    def is_valid(self, top_left, bottom_right, S, all_robots, map, conflict):
        """
        Determine if a given subproblem is valid. A subproblem is valid if:
        1. It covers at least the conflict in question
        2. It matches its template's obstacle layout
        3. It does not overlap with any subproblem in S
        """
        max_x = bottom_right[0]
        min_x = top_left[0]
        min_y = top_left[1]
        max_y = bottom_right[1]

        # check that we are in bounds of our environment
        if min_x < 0 or min_y < 0 or max_x > len(map)-1 or max_y > len(map[0])-1: 
            # print("out of bounds")
            return False

        count = 0
        for x in range(min_x, max_x+1):
            for y in range(min_y, max_y+1):
                if map[x][y]: 
                    count += 1
                    if self.type != 25: return False
                if self.type == 25 and count > 1: 
                    # print("More than one obstacle detected")
                    return False

        if self.type == 25:
            # Check that the obstacle is in the correct place using exclusive or
            if max_x - min_x == 1:
                if (map[top_left[0]][bottom_right[1]-2]) == (map[top_left[0]+1][bottom_right[1]-2]):
                    # invalid
                    # print("obstacle in the wrong place")
                    return False
            else:
                if (map[top_left[0]+2][bottom_right[1]]) == (map[top_left[0]+2][bottom_right[1]-1]):
                    # invalid
                    # print("obstacle in the wrong place")
                    return False

        # check that all robots in conflict are in the subproblem
        robots_in_conflict = conflict.get_robot_idxs()
        for robot_idx in robots_in_conflict:
            pos = all_robots[robot_idx].get_current_position()
            x = pos[0]
            y = pos[1]

            if x > max_x or x < min_x or y > max_y or y < min_y:
                # print(f"robot {robot_idx} is left out at position {pos}")
                return False
            
        # check that there is no overlap with any other subproblem in S
        x = set(range(min_x,max_x+1))
        y = set(range(min_y,max_y+1))
        for s in S:
            s_top_left = s.top_left
            s_bottom_right = s.bottom_right
            s_max_x = s_bottom_right[0]
            s_min_x = s_top_left[0]
            s_min_y = s_top_left[1]
            s_max_y = s_bottom_right[1]

            s_x = set(range(s_min_x,s_max_x+1))
            s_y = set(range(s_min_y,s_max_y+1))


            x_overlap = s_x.intersection(x)  
            y_overlap = s_y.intersection(y)  
            if len(x_overlap) > 0 and len(y_overlap) > 0:
                return False
            
        return True


if __name__ == "__main__":
    # TODO: write test case of subproblem
    pass