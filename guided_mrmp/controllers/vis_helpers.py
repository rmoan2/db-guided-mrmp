"""
Various helper functions for visualization
"""
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from guided_mrmp.controllers.utils import ego_to_global_roomba, get_grid_cell_location

def plot_roomba(x, y, yaw, color, fill, radius):
    """
    inputs:
        x (float): x position
        y (float): y position
        yaw (float): angle in radians
        color (rgb): color of the robot
        fill (bool): fill the robot with color or not
        radius (float): radius of the robot
    """
    fig = plt.gcf()
    ax = fig.gca()
    if fill: alpha = .3
    else: alpha = 1
    circle = plt.Circle((x, y), radius, color=color, fill=fill, alpha=alpha)
    ax.add_patch(circle)

    # Plot direction marker
    dx = 1 * np.cos(yaw)
    dy = 1 * np.sin(yaw)
    ax.arrow(x, y, dx, dy, head_width=0.1, head_length=0.1, fc='r', ec='r')

def draw_grid(num_robots, radius, grid_size, cell_size, env, trajs, state, grid_origin, obstacle_map, robots_in_conflict):
    """
    params:
        - initial_guess (dict): the initial guess for the optimization problem
        - state (list): list of robot states
        - grid_origin (tuple): top left corner of the grid
        - obstacle_map (bool array): the obstacle map of the grid
        - num_robots (int): number of robots that are in conflict
    """

    circle_obs = env.circle_obs
    rect_obs = env.rect_obs
    x_range = env.boundary[0]
    y_range = env.boundary[1]
    
    # draw the whole environment with the local grid drawn on top
    import matplotlib.pyplot as plt
    import matplotlib.cm as cm

    # Plot the current state of each robot using the current state
    colors = cm.rainbow(np.linspace(0, 1, num_robots))
    for i in range(num_robots):
        plot_roomba(state[i][0], state[i][1], state[i][2], colors[i], False, radius)

    # draw the horizontal and vertical lines of the grid
    for i in range(grid_size + 1):
        # Draw vertical lines
        plt.plot([grid_origin[0] + i * cell_size, grid_origin[0] + i * cell_size], 
                    [grid_origin[1], grid_origin[1] + grid_size * cell_size], 'k-')
        # Draw horizontal lines
        plt.plot([grid_origin[0], grid_origin[0] + grid_size * cell_size], 
                    [grid_origin[1] +  i * cell_size, grid_origin[1] + i * cell_size], 'k-')

    # draw the obstacles
    for obs in circle_obs:
        circle = plt.Circle((obs[0], obs[1]), obs[2], color='red', fill=False)
        plt.gca().add_artist(circle)

    for obs in rect_obs:
        rect = plt.Rectangle((obs[0], obs[1]), obs[2], obs[3], color='k', fill=True)
        plt.gca().add_artist(rect)

    # if a cell is true in the obstacle map, that cell is an obstacle.
    # fill that cell with a translucent red color
    for i in range(grid_size):
        for j in range(grid_size):
            if obstacle_map[i][j]:
                x, y = get_grid_cell_location(i, j, grid_origin, cell_size)
                # create a square with the top left corner at (x - cell_size/2, y - cell_size/2)
                square = plt.Rectangle((x - cell_size/2, y - cell_size/2), cell_size, cell_size, color='r', alpha=0.3)
                plt.gca().add_artist(square)


    # plot the robots' continuous space subgoals
    for idx in robots_in_conflict:
        traj = ego_to_global_roomba(state[idx], trajs[idx])
        x = traj[0][-1]
        y = traj[1][-1]
        plt.plot(x, y, '^', color=colors[idx])
        circle1 = plt.Circle((x, y), radius, color=colors[idx], fill=False)
        plt.gca().add_artist(circle1)

    # set the size of the plot to be 10x10
    plt.xlim(x_range[0], x_range[1])
    plt.xlim(y_range[0], y_range[1])

    # force equal aspect ratio
    plt.gca().set_aspect('equal', adjustable='box')

    # title
    plt.title("Discrete Solution")

    plt.show()

def draw_chosen_subp(num_robots, radius, grid_size, cell_size, env,trajs, state, grid_origin, obstacle_map, robots_in_conflict, subp_bottom_left, subp_top_right):
    """
    params:
        - state (list): list of robot states
        - grid_origin (tuple): top left corner of the grid
        - obstacle_map (bool array): the obstacle map of the grid
        - num_robots (int): number of robots that are in conflict
    """
    circle_obs = env.circle_obs
    rect_obs = env.rect_obs
    x_range = env.boundary[0]
    y_range = env.boundary[1]
    
    # draw the whole environment with the local grid drawn on top
    colors = cm.rainbow(np.linspace(0, 1, num_robots))

    for i in range(num_robots):
        plot_roomba(state[i][0], state[i][1], state[i][2], colors[i], False, radius)

    # draw the horizontal and vertical lines of the grid
    for i in range(grid_size + 1):
        # Draw vertical lines
        plt.plot([grid_origin[0] + i * cell_size, grid_origin[0] + i * cell_size], 
                    [grid_origin[1], grid_origin[1] + grid_size * cell_size], 'k-')
        # Draw horizontal lines
        plt.plot([grid_origin[0], grid_origin[0] + grid_size * cell_size], 
                    [grid_origin[1] +  i * cell_size, grid_origin[1] + i * cell_size], 'k-')

    # draw a blue rectangle with the bottom left corner at subp_bottom_left and top right corner at subp_top_right
    rect = plt.Rectangle(subp_bottom_left, subp_top_right[0] - subp_bottom_left[0], subp_top_right[1] - subp_bottom_left[1], color='b', fill=False, linewidth=4)
    plt.gca().add_artist(rect)


    # draw the obstacles
    for obs in circle_obs:
        circle = plt.Circle((obs[0], obs[1]), obs[2], color='red', fill=False)
        plt.gca().add_artist(circle)

    for obs in rect_obs:
        rect = plt.Rectangle((obs[0], obs[1]), obs[2], obs[3], color='k', fill=True)
        plt.gca().add_artist(rect)

    # if a cell is true in the obstacle map, that cell is an obstacle.
    # fill that cell with a translucent red color
    for i in range(grid_size):
        for j in range(grid_size):
            if obstacle_map[i][j]:
                x, y = get_grid_cell_location(i, j, grid_origin, cell_size)
                # create a square with the top left corner at (x - cell_size/2, y - cell_size/2)
                square = plt.Rectangle((x - cell_size/2, y - cell_size/2), cell_size, cell_size, color='r', alpha=0.3)
                plt.gca().add_artist(square)


    # plot the robots' continuous space subgoals
    # for idx in robots_in_conflict:
    #     traj = ego_to_global_roomba(state[idx], trajs[idx])
    #     x = traj[0][-1]
    #     y = traj[1][-1]
    #     plt.plot(x, y, '^', color=colors[idx])
    #     circle1 = plt.Circle((x, y), radius, color=colors[idx], fill=False)
    #     plt.gca().add_artist(circle1)

    # set the size of the plot to be 10x10
    plt.xlim(x_range[0], x_range[1])
    plt.xlim(y_range[0], y_range[1])

    # force equal aspect ratio
    plt.gca().set_aspect('equal', adjustable='box')

    # title
    plt.title("Discrete Solution")

    # plt.savefig(f"example/sim_00{time}5.png")
    plt.show()

def draw_temp_goals(num_robots, radius, grid_size, cell_size, env,trajs, state, grid_origin, obstacle_map, robots_in_conflict, subp_bottom_left, subp_top_right, temp_goals, guide_paths):
    """
    params:
        - initial_guess (dict): the initial guess for the optimization problem
        - state (list): list of robot states
        - grid_origin (tuple): top left corner of the grid
        - obstacle_map (bool array): the obstacle map of the grid
        - num_robots (int): number of robots that are in conflict
    """
    circle_obs = env.circle_obs
    rect_obs = env.rect_obs
    x_range = env.boundary[0]
    y_range = env.boundary[1]
    
    # draw the whole environment with the local grid drawn on top
    colors = cm.rainbow(np.linspace(0, 1, num_robots))

    for i in range(num_robots):
        plot_roomba(state[i][0], state[i][1], state[i][2], colors[i], False, radius)

    # draw the horizontal and vertical lines of the grid
    for i in range(grid_size + 1):
        # Draw vertical lines
        plt.plot([grid_origin[0] + i * cell_size, grid_origin[0] + i * cell_size], 
                    [grid_origin[1], grid_origin[1] + grid_size * cell_size], 'k-')
        # Draw horizontal lines
        plt.plot([grid_origin[0], grid_origin[0] + grid_size * cell_size], 
                    [grid_origin[1] +  i * cell_size, grid_origin[1] + i * cell_size], 'k-')

    # draw the obstacles
    for obs in circle_obs:
        circle = plt.Circle((obs[0], obs[1]), obs[2], color='red', fill=False)
        plt.gca().add_artist(circle)

    for obs in rect_obs:
        rect = plt.Rectangle((obs[0], obs[1]), obs[2], obs[3], color='k', fill=True)
        plt.gca().add_artist(rect)

    # if a cell is true in the obstacle map, that cell is an obstacle.
    # fill that cell with a translucent red color
    for i in range(grid_size):
        for j in range(grid_size):
            if obstacle_map[i][j]:
                x, y = get_grid_cell_location(i, j, grid_origin, cell_size)
                # create a square with the top left corner at (x - cell_size/2, y - cell_size/2)
                square = plt.Rectangle((x - cell_size/2, y - cell_size/2), cell_size, cell_size, color='r', alpha=0.3)
                plt.gca().add_artist(square)



    if subp_bottom_left is not None and subp_top_right is not None:
        # draw a blue rectangle with the bottom left corner at subp_bottom_left and top right corner at subp_top_right
        rect = plt.Rectangle(subp_bottom_left, subp_top_right[0] - subp_bottom_left[0], subp_top_right[1] - subp_bottom_left[1], color='b', fill=False, linewidth=4)
        plt.gca().add_artist(rect)

    for idx, r in enumerate(robots_in_conflict):
        path = guide_paths[r]
        temp_goal = temp_goals[idx]
        color = colors[r]
        # plot guide path with a dashed line
        plt.plot(path[0], path[1], '--', color=color)

        # plot the temp goal with a solid circle
        plt.plot(temp_goal[0], temp_goal[1], 'o', color=color)
    


    # plot the robots' continuous space subgoals
    for idx in robots_in_conflict:
        # traj = ego_to_global_roomba(state[idx], trajs[idx])
        traj = trajs[idx]
        x = traj[0][-1]
        y = traj[1][-1]
        plt.plot(x, y, '^', color=colors[idx])
        circle1 = plt.Circle((x, y), radius, color=colors[idx], fill=False)
        plt.gca().add_artist(circle1)

    # set the size of the plot to be 10x10
    plt.xlim(x_range[0], x_range[1])
    plt.xlim(y_range[0], y_range[1])

    # force equal aspect ratio
    plt.gca().set_aspect('equal', adjustable='box')

    # title
    plt.title("Discrete Solution")

    # plt.savefig(f"example/sim_00{time}5.png")
    plt.show()

def plot_background(num_robots, radius, grid_size, cell_size, env,trajs, initial_guess, state, grid_origin, obstacle_map, robots_in_conflict, subp_bottom_left, subp_top_right, grid_points):
    circle_obs = env.circle_obs
    rect_obs = env.rect_obs
    x_range = env.boundary[0]
    y_range = env.boundary[1]
    plt.xlim(x_range[0], x_range[1])
    plt.xlim(y_range[0], y_range[1])
    
    # draw the whole environment with the local grid drawn on top
    colors = cm.rainbow(np.linspace(0, 1, num_robots))

    for i in range(num_robots):
        plot_roomba(state[i][0], state[i][1], state[i][2], colors[i], False, radius)

    # draw the horizontal and vertical lines of the grid
    for i in range(grid_size + 1):
        # Draw vertical lines
        plt.plot([grid_origin[0] + i * cell_size, grid_origin[0] + i * cell_size], 
                    [grid_origin[1], grid_origin[1] + grid_size * cell_size], 'k-')
        # Draw horizontal lines
        plt.plot([grid_origin[0], grid_origin[0] + grid_size * cell_size], 
                    [grid_origin[1] +  i * cell_size, grid_origin[1] + i * cell_size], 'k-')

    # draw the obstacles
    for obs in circle_obs:
        circle = plt.Circle((obs[0], obs[1]), obs[2], color='red', fill=False)
        plt.gca().add_artist(circle)

    for obs in rect_obs:
        rect = plt.Rectangle((obs[0], obs[1]), obs[2], obs[3], color='k', fill=True)
        plt.gca().add_artist(rect)

    # if a cell is true in the obstacle map, that cell is an obstacle.
    # fill that cell with a translucent red color
    for i in range(grid_size):
        for j in range(grid_size):
            if obstacle_map[i][j]:
                x, y = get_grid_cell_location(i, j, grid_origin, cell_size)
                # create a square with the top left corner at (x - cell_size/2, y - cell_size/2)
                square = plt.Rectangle((x - cell_size/2, y - cell_size/2), cell_size, cell_size, color='r', alpha=0.3)
                plt.gca().add_artist(square)



def draw_grid_solution(num_robots, radius, grid_size, cell_size, env,trajs, initial_guess, state, grid_origin, obstacle_map, robots_in_conflict, subp_bottom_left, subp_top_right, grid_points):
    """
    params:
        - initial_guess (dict): the initial guess for the optimization problem
        - state (list): list of robot states
        - grid_origin (tuple): top left corner of the grid
        - obstacle_map (bool array): the obstacle map of the grid
        - num_robots (int): number of robots that are in conflict
    """
    circle_obs = env.circle_obs
    rect_obs = env.rect_obs
    x_range = env.boundary[0]
    y_range = env.boundary[1]
    
    # draw the whole environment with the local grid drawn on top
    colors = cm.rainbow(np.linspace(0, 1, num_robots))

    for i in range(num_robots):
        plot_roomba(state[i][0], state[i][1], state[i][2], colors[i], False, radius)

    # draw the horizontal and vertical lines of the grid
    for i in range(grid_size + 1):
        # Draw vertical lines
        plt.plot([grid_origin[0] + i * cell_size, grid_origin[0] + i * cell_size], 
                    [grid_origin[1], grid_origin[1] + grid_size * cell_size], 'k-')
        # Draw horizontal lines
        plt.plot([grid_origin[0], grid_origin[0] + grid_size * cell_size], 
                    [grid_origin[1] +  i * cell_size, grid_origin[1] + i * cell_size], 'k-')

    # draw the obstacles
    for obs in circle_obs:
        circle = plt.Circle((obs[0], obs[1]), obs[2], color='red', fill=False)
        plt.gca().add_artist(circle)

    for obs in rect_obs:
        rect = plt.Rectangle((obs[0], obs[1]), obs[2], obs[3], color='k', fill=True)
        plt.gca().add_artist(rect)

    # if a cell is true in the obstacle map, that cell is an obstacle.
    # fill that cell with a translucent red color
    for i in range(grid_size):
        for j in range(grid_size):
            if obstacle_map[i][j]:
                x, y = get_grid_cell_location(i, j, grid_origin, cell_size)
                # create a square with the top left corner at (x - cell_size/2, y - cell_size/2)
                square = plt.Rectangle((x - cell_size/2, y - cell_size/2), cell_size, cell_size, color='r', alpha=0.3)
                plt.gca().add_artist(square)


    # for i in range(num_robots):
    if len(initial_guess) > 0:
        for robot_idx, i  in enumerate(robots_in_conflict):
            x = initial_guess[robot_idx*3, :]
            y = initial_guess[robot_idx*3 + 1, :]
            plt.plot(x, y, 'x', color=colors[i])

    if len(grid_points) > 0:
        for i in range(len(grid_points)):
            for point in grid_points[i]:
                x, y = grid_origin + np.array(point)*cell_size + cell_size/2
                plt.plot(x, y, color=colors[i], marker='x', markersize=10)

    if subp_bottom_left is not None and subp_top_right is not None:
        # draw a blue rectangle with the bottom left corner at subp_bottom_left and top right corner at subp_top_right
        rect = plt.Rectangle(subp_bottom_left, subp_top_right[0] - subp_bottom_left[0], subp_top_right[1] - subp_bottom_left[1], color='b', fill=False, linewidth=4)
        plt.gca().add_artist(rect)

    # plot the robots' continuous space subgoals
    # for idx in robots_in_conflict:
    #     traj = ego_to_global_roomba(state[idx], trajs[idx])
    #     x = traj[0][-1]
    #     y = traj[1][-1]
    #     plt.plot(x, y, '^', color=colors[idx])
    #     circle1 = plt.Circle((x, y), radius, color=colors[idx], fill=False)
    #     plt.gca().add_artist(circle1)

    # set the size of the plot to be 10x10
    plt.xlim(x_range[0], x_range[1])
    plt.xlim(y_range[0], y_range[1])

    # force equal aspect ratio
    plt.gca().set_aspect('equal', adjustable='box')

    # title
    plt.title("Discrete Solution")

    # plt.savefig(f"example/sim_00{time}5.png")
    plt.show()

def draw_grid_world_solution(num_robots, radius, grid_size, cell_size, env,trajs, initial_guess, state, grid_origin, obstacle_map, robots_in_conflict, subp_bottom_left, subp_top_right, grid_points):
    """
    params:
        - initial_guess (dict): the initial guess for the optimization problem
        - state (list): list of robot states
        - grid_origin (tuple): top left corner of the grid
        - obstacle_map (bool array): the obstacle map of the grid
        - num_robots (int): number of robots that are in conflict
    """
    # plot_background(num_robots, radius, grid_size, cell_size, env,trajs, initial_guess, state, grid_origin, obstacle_map, robots_in_conflict, subp_bottom_left, subp_top_right, grid_points)

    colors = cm.rainbow(np.linspace(0, 1, num_robots))

    # if len(grid_points) > 0:
    #     for i in range(len(grid_points)):
    #         color = colors[robots_in_conflict[i]]
    #         for point in grid_points[i]:
    #             x, y = grid_origin + np.array(point)*cell_size + cell_size/2
    #             plt.plot(x, y, color=color, marker='x', markersize=10)

    # get the length of the longest grid solution
    max_len = max([len(grid_points[i]) for i in range(len(grid_points))])

    for i in range(max_len):
        plot_background(num_robots, radius, grid_size, cell_size, env,trajs, initial_guess, state, grid_origin, obstacle_map, robots_in_conflict, subp_bottom_left, subp_top_right, grid_points)

        if subp_bottom_left is not None and subp_top_right is not None:
            print()
            # draw a blue rectangle with the bottom left corner at subp_bottom_left and top right corner at subp_top_right
            rect = plt.Rectangle(subp_bottom_left, subp_top_right[0] - subp_bottom_left[0], subp_top_right[1] - subp_bottom_left[1], color='b', fill=False, linewidth=4)
            plt.gca().add_artist(rect)

        for j in range(len(grid_points)):
            if i < len(grid_points[j]):
                color = colors[robots_in_conflict[j]]
                point = grid_points[j][i]
                x, y = grid_origin + np.array(point)*cell_size + cell_size/2
                plt.plot(x, y, color=color, marker='x', markersize=10)
            else:
                color = colors[robots_in_conflict[j]]
                point = grid_points[j][-1]
                x, y = grid_origin + np.array(point)*cell_size + cell_size/2
                plt.plot(x, y, color=color, marker='x', markersize=10)

    # plot the robots' continuous space subgoals
    # for idx in robots_in_conflict:
    #     traj = ego_to_global_roomba(state[idx], trajs[idx])
    #     x = traj[0][-1]
    #     y = traj[1][-1]
    #     plt.plot(x, y, '^', color=colors[idx])
    #     circle1 = plt.Circle((x, y), radius, color=colors[idx], fill=False)
    #     plt.gca().add_artist(circle1)

    

        # force equal aspect ratio
        plt.gca().set_aspect('equal', adjustable='box')

        # title
        plt.title("Discrete Solution")

        # plt.savefig(f"example/sim_00{time}5.png")
        plt.show()

def plot_trajs(num_robots, paths, env, state, traj1, traj2, radius):
    """
    Plot the trajectories of two robots.
    """
    x_range = env.boundary[0]
    y_range = env.boundary[1]

    import matplotlib.pyplot as plt
    import matplotlib.cm as cm

    # Plot the current state of each robot using the most recent values from
    # x_history, y_history, and h_history
    colors = cm.rainbow(np.linspace(0, 1, num_robots))

    for i in range(num_robots):
        plot_roomba(state[i][0], state[i][1], state[i][2], colors[i], False, radius)

    # plot the goal of each robot with solid circle
    for i in range(num_robots):
        x, y, theta = paths[i][:, -1]
        plt.plot(x, y, 'o', color=colors[i])
        circle1 = plt.Circle((x, y), radius, color=colors[i], fill=False)
        plt.gca().add_artist(circle1)
    
    for i in range(traj1.shape[1]):
        circle1 = plt.Circle((traj1[0, i], traj1[1, i]), radius, color='k', fill=False)
        plt.gca().add_artist(circle1)

    for i in range(traj2.shape[1]):
        circle2 = plt.Circle((traj2[0, i], traj2[1, i]), radius, color='k', fill=False)
        plt.gca().add_artist(circle2)

    # set the size of the plot to be 10x10
    plt.xlim(x_range[0], x_range[1])
    plt.xlim(y_range[0], y_range[1])

    # force equal aspect ratio
    plt.gca().set_aspect('equal', adjustable='box')
    

    plt.show()

def plot_old_and_new_guides(radius, old, new, x_grid_sol, y_grid_sol):
    """
    Plot the old and new guide paths side by side
    """
    old_x, old_y, old_theta = old
    new_x, new_y, new_theta = new

    import matplotlib.pyplot as plt
    
    for i in range(len(old_x)):
        plot_roomba(old_x[i], old_y[i], old_theta[i], 'b', False, radius)
    for i in range(len(new_x)):
        plot_roomba(new_x[i], new_y[i], new_theta[i], 'r', False, radius)

    
    plt.plot(x_grid_sol, y_grid_sol, 'x', color='g')

    # force equal aspect ratio
    plt.gca().set_aspect('equal', adjustable='box')

    plt.show()

def plot_following_ref_exactly(num_robots, paths, radius, time):
    """
    Plot the robots following the guides exactly
    """
    import matplotlib.pyplot as plt
    import matplotlib.cm as cm

    colors = cm.rainbow(np.linspace(0, 1, num_robots))

    # get the length of the longest guide path
    max_len = max([len(paths[i][0]) for i in range(num_robots)])

    for i in range(max_len):

        # plot all guide paths with -- 
        for j in range(num_robots):
            plt.plot(paths[j][0], paths[j][1], '--', color=colors[j])


        for j in range(num_robots):
            if i < len(paths[j][0]):
                plot_roomba(paths[j][0][i], paths[j][1][i], paths[j][2][i], colors[j], False, radius)
            else:
                plot_roomba(paths[j][0][-1], paths[j][1][-1], paths[j][2][-1], colors[j], False, radius)

        # force equal aspect ratio
        plt.gca().set_aspect('equal', adjustable='box')
        # plt.savefig(f"example/sim_00{time}05.png")
        # plt.show()
        