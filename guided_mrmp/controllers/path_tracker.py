import numpy as np
import matplotlib.pyplot as plt

# from guided_mrmp.controllers.utils import compute_path_from_wp, get_ref_trajectory
from guided_mrmp.controllers.multi_mpc import MultiMPC
from guided_mrmp.utils import Roomba

from scipy.interpolate import interp1d


def compute_path_from_wp(start_xp, start_yp, step=0.1):
    """
    params:
        start_xp (array-like): 1D array of x-positions
        start_yp (array-like): 1D array of y-positions
        step (float): interpolation step size 

    output:
        ndarray of shape (3,N) representing the  path as x,y,heading
    """
    final_xp = []
    final_yp = []
    delta = step  # [m]
    for idx in range(len(start_xp) - 1):
        # find the distance between consecutive waypoints
        section_len = np.sum(
            np.sqrt(
                np.power(np.diff(start_xp[idx : idx + 2]), 2)
                + np.power(np.diff(start_yp[idx : idx + 2]), 2)
            )
        )

        # how many interpolated points are needed to reach the next waypoint
        interp_range = np.linspace(0, 1, np.floor(section_len / delta).astype(int))  

        # interpolate between waypoints
        fx = interp1d(np.linspace(0, 1, 2), start_xp[idx : idx + 2], kind=1)
        fy = interp1d(np.linspace(0, 1, 2), start_yp[idx : idx + 2], kind=1)

        # append the interpolated points to the final path 
        final_xp = np.append(final_xp, fx(interp_range)[1:])
        final_yp = np.append(final_yp, fy(interp_range)[1:])
    dx = np.append(0, np.diff(final_xp))
    dy = np.append(0, np.diff(final_yp))
    theta = np.arctan2(dy, dx)
    print(f"theta = {theta}")
    return np.vstack((final_xp, final_yp, theta))


def fix_angle_reference(angle_ref, angle_init):
    """
    Removes jumps greater than 2PI to smooth the heading.

    Args:
        angle_ref (array-like): Reference angles
        angle_init (float): Initial angle

    Returns:
        array-like: Smoothed reference angles
    """
    diff_angle = angle_ref - angle_init
    diff_angle = np.unwrap(diff_angle)
    return angle_init + diff_angle


def get_ref_trajectory(state, path, T, DT, path_visited_points=[]):
    """
    Generates a reference trajectory for the Roomba.

    Args:
        state (array-like): Current state [x, y, theta]
        path (ndarray): Path points [x, y, theta] in the global frame
        path_visited_points (array-like): Visited path points [[x, y], [x, y], ...]
        target_v (float): Desired speed
        T (float): Control horizon duration
        DT (float): Control horizon time-step

    Returns:
        ndarray: Reference trajectory [x_k, y_k, theta_k] in the ego frame
    """
    K = T/DT
    xref = np.zeros((3, K+1))  # Reference trajectory for [x, y, theta]

    # Find the last visited point
    last_visited_idx = 0 if path_visited_points == [] else path_visited_points[-1]

    # Find the spatially closest point after the last visited point
    next_ind = last_visited_idx + 1
    path_visited_points.append(next_ind)

    # print(f"guide path = {path}")
    
    if K +1  + next_ind < len(path[0]):
        # return the next k points on the path
        xref[0,:] = path[0, next_ind:next_ind+K+1]
        xref[1,:] = path[1, next_ind:next_ind+K+1]
        xref[2,:] = path[2, next_ind:next_ind+K+1]

    else:
        xref[0, 0:len(path[0]) - next_ind] = path[0, next_ind:]
        xref[1, 0:len(path[0]) - next_ind] = path[1, next_ind:]
        xref[2, 0:len(path[0]) - next_ind] = path[2, next_ind:]

        # Fill the rest of the trajectory with the last point
        xref[0, len(path[0]) - next_ind:] = path[0, -1]
        xref[1, len(path[0]) - next_ind:] = path[1, -1]
        xref[2, len(path[0]) - next_ind:] = path[2, -1]

    # Transform to ego frame
    # dx = xref[0, :] - state[0]
    # dy = xref[1, :] - state[1]
    # xref[0, :] = dx * np.cos(-state[2]) - dy * np.sin(-state[2])  # X
    # xref[1, :] = dy * np.cos(-state[2]) + dx * np.sin(-state[2])  # Y

    # if next_ind < len(path[0]):
    #     xref[2, :] = path[2, next_ind] - state[2]  # Theta
    # else:
    #     xref[2, :] = path[2, -1] - state[2]
    

    # Normalize the angles
    # xref[2, :] = (xref[2, :] + np.pi) % (2.0 * np.pi) - np.pi
    # xref[2, :] = fix_angle_reference(xref[2, :], xref[2, 0])


    # print(f"x_ref = {xref}")
    return xref, path_visited_points


def desired_command_and_trajectory(x0_:np.array, N_, guide_path, points_visited):

    # Find the last visited point
    last_visited_idx = 0 if points_visited == [] else points_visited[-1]


    next_ind = last_visited_idx + 1
    points_visited.append(next_ind)

    



    x_ = np.zeros((3, N_+1))
    x_[:,0] = x0_

    for i in range(N_):
        if next_ind+i >= len(guide_path[0]):
            x_ref_ = guide_path[0, -1]
            y_ref_ = guide_path[1, -1]
            theta_ref_ = guide_path[2, -1]
        else:
            x_ref_ = guide_path[0, next_ind+i]
            y_ref_ = guide_path[1, next_ind+i]
            theta_ref_ = guide_path[2, next_ind+i]
        x_[:,i+1] = np.array([x_ref_, y_ref_, theta_ref_])

    return x_, points_visited


# Classes
class PathTracker:
    def __init__(self, initial_position, dynamics, target_v, T, DT, waypoints, settings):
        """
        Initializes the PathTracker object.
        Parameters:
        - initial_position: The initial position of the robot [x, y, heading].
        - dynamics: The dynamics model of the robot.
        - target_v: The target velocity of the robot.
        - T: The time horizon for the model predictive control (MPC).
        - DT: The time step for the MPC.
        - waypoints: A list of waypoints defining the desired path.
        """
        # State of the robot [x,y, heading]
        self.state = initial_position
        self.dynamics = dynamics
        self.T = T
        self.DT = DT
        self.target_v = target_v

        # helper variable to keep track of mpc output
        # starting condition is 0,0
        self.control = np.zeros(2)

        self.K = int(T / DT)

        # For a circular robot (easy dynamics)
        self.mpc = MultiMPC(1,
                            dynamics, 
                            T, 
                            DT, 
                            settings, 
                            circle_obs=[[5,0,.25]],
                            rectangle_obs=[])

        # Path from waypoint interpolation
        self.path = compute_path_from_wp(waypoints[0], waypoints[1], .1)

        # Helper variables to keep track of the sim
        self.sim_time = 0
        self.x_history = []
        self.y_history = []
        self.v_history = []
        self.h_history = []
        self.a_history = []
        self.d_history = []
        self.optimized_trajectory = None

        self.path_visited_points = []

        # Initialise plot
        # plt.style.use("ggplot")
        # self.fig = plt.figure()
        # plt.ion()
        # plt.show()

    def ego_to_global(self, mpc_out):
        """
        transforms optimized trajectory XY points from ego (robot) reference
        into global (map) frame

        Args:
            mpc_out ():
        """
        trajectory = np.zeros((2, self.K))
        trajectory[:, :] = mpc_out[0:2, 1:]
        Rotm = np.array(
            [
                [np.cos(self.state[3]), np.sin(self.state[3])],
                [-np.sin(self.state[3]), np.cos(self.state[3])],
            ]
        )
        trajectory = (trajectory.T.dot(Rotm)).T
        trajectory[0, :] += self.state[0]
        trajectory[1, :] += self.state[1]
        return trajectory
    
    def ego_to_global_roomba(self, mpc_out):
        """
        Transforms optimized trajectory XY points from ego (robot) reference
        into global (map) frame.

        Args:
            mpc_out (numpy array): Optimized trajectory points in ego reference frame.

        Returns:
            numpy array: Transformed trajectory points in global frame.
        """
        # Extract x, y, and theta from the state
        x = self.state[0]
        y = self.state[1]
        theta = self.state[2]

        # Rotation matrix to transform points from ego frame to global frame
        Rotm = np.array([
            [np.cos(theta), -np.sin(theta)],
            [np.sin(theta), np.cos(theta)]
        ])

        # Initialize the trajectory array (only considering XY points)
        trajectory = mpc_out[0:2, :]

        # Apply rotation to the trajectory points
        trajectory = Rotm.dot(trajectory)

        # Translate the points to the robot's position in the global frame
        trajectory[0, :] += x
        trajectory[1, :] += y

        return trajectory

    # def shift(T, t0, x0, u, x_n, f):
        # f_value = f(x0, u[0])
        # st = x0 + T*f_value
        # t = t0 + T
        # u_end = np.concatenate((u[1:], u[-1:]))
        # x_n = np.concatenate((x_n[1:], x_n[-1:]))
        # return t, st, u_end, x_n

    def get_next_control(self, state, show_plots=False):
        # optimization loop
        # start=time.time()

        print(f"state = {state}")

        # Get Reference_traj -> inputs are in worldframe
        # target, self.path_visited_points = get_ref_trajectory(np.array(state), np.array(self.path), self.T, self.DT, self.path_visited_points)
        target, self.path_visited_points = desired_command_and_trajectory(np.array(state), self.K, np.array(self.path), self.path_visited_points)

        print(f"target = {target}")
        print(f"points_visited = {self.path_visited_points}")


        # dynamycs w.r.t robot frame
        # curr_state = np.array([0, 0, self.state[2], 0])
        curr_state = np.array([state])
        # curr_state = np.array([state])
        x_mpc, u_mpc = self.mpc.step(
            curr_state,
            [target],
            [self.control]
        )
        
        # only the first one is used to advance the simulation
        self.control[:] = [u_mpc[0, 0], u_mpc[1, 0]]

        return x_mpc, self.control

    def shift(self, DT, t0, x0, u, x_n, f):
        f_value = f(x0, u)
        st = x0 + DT*f_value
        t = t0 + DT
        u_end = np.concatenate((u[1:], u[-1:]))
        x_n = np.concatenate((x_n[1:], x_n[-1:]))
        return t, st, u_end, x_n 

    def run(self, show_plots=False):
        """
        Run the path tracker algorithm.
        Parameters:
        - show_plots (bool): Flag indicating whether to show plots during the simulation. Default is False.
        Returns:
        - numpy.ndarray: Array containing the history of x, y, and h coordinates.
        """

        # Add the initial state to the histories
        self.x_history.append(self.state[0])
        self.y_history.append(self.state[1])
        self.h_history.append(self.state[2])
        if show_plots: self.plot_sim()
        
        while 1:
            if (np.sqrt((self.state[0] - self.path[0, -1]) ** 2 + (self.state[1] - self.path[1, -1]) ** 2) < 0.1):
                print("Success! Goal Reached")
                return np.asarray([self.x_history, self.y_history, self.h_history])
            x_mpc, controls = self.get_next_control(self.state)
            print(f"self.state = {self.state}")
            print(f"x_mpc = {x_mpc}")
            # next_state = self.dynamics.next_state(self.state, [self.control[0], self.control[1]], self.DT)


            f_np = lambda x_, u_: np.array([u_[0]*np.cos(x_[2]), u_[0]*np.sin(x_[2]), u_[1]])

            t0, current_state, u0, next_states = self.shift(self.DT, 0, self.state, controls, x_mpc, f_np)


            self.state = current_state

            self.x_history.append(self.state[0])
            self.y_history.append(self.state[1])
            self.h_history.append(self.state[2])
            
            # use the optimizer output to preview the predicted state trajectory
            # self.optimized_trajectory = self.ego_to_global(x_mpc.value)
            if show_plots: self.optimized_trajectory =  x_mpc
            if show_plots: self.plot_sim()
            
    def plot_sim(self):
        self.sim_time = self.sim_time + self.DT
        # self.x_history.append(self.state[0])
        # self.y_history.append(self.state[1])
        # self.v_history.append(self.control[0])
        self.h_history.append(self.state[2])
        self.d_history.append(self.control[1])

        plt.clf()

        grid = plt.GridSpec(2, 3)

        plt.subplot(grid[0:2, 0:2])
        plt.title("MPC Simulation \n" + "Simulation elapsed time {}s".format(self.sim_time))

        # plot a circle at 5,5 with radius .5
        circle = plt.Circle((5, 0), .5, color='r', fill=False)
        plt.gca().add_artist(circle)


        plt.plot(
            self.path[0, :],
            self.path[1, :],
            c="tab:orange",
            marker=".",
            label="reference track",
        )

        plt.plot(
            self.x_history,
            self.y_history,
            c="tab:blue",
            marker=".",
            alpha=0.5,
            label="vehicle trajectory",
        )

        if self.optimized_trajectory is not None:
            plt.plot(
                self.optimized_trajectory[0, :],
                self.optimized_trajectory[1, :],
                c="tab:green",
                marker="+",
                alpha=0.5,
                label="mpc opt trajectory",
            )


        plot_roomba(self.x_history[-1], self.y_history[-1], self.h_history[-1])


        plt.ylabel("map y")
        plt.yticks(np.arange(min(self.path[1, :]) - 1.0, max(self.path[1, :] + 1.0) + 1, 1.0))
        plt.xlabel("map x")
        plt.xticks(np.arange(min(self.path[0, :]) - 1.0, max(self.path[0, :] + 1.0) + 1, 1.0))
        plt.axis("equal")
        # plt.legend()

        plt.subplot(grid[0, 2])
        # plt.title("Linear Velocity {} m/s".format(self.v_history[-1]))
        # plt.plot(self.a_history, c="tab:orange")
        # locs, _ = plt.xticks()
        # plt.xticks(locs[1:], locs[1:] * DT)
        # plt.ylabel("a(t) [m/ss]")
        # plt.xlabel("t [s]")

        plt.subplot(grid[1, 2])
        # plt.title("Angular Velocity {} m/s".format(self.w_history[-1]))
        plt.plot(np.degrees(self.d_history), c="tab:orange")
        plt.ylabel("gamma(t) [deg]")
        locs, _ = plt.xticks()
        plt.xticks(locs[1:], locs[1:] * DT)
        plt.xlabel("t [s]")

        plt.tight_layout()

        ax = plt.gca()
        ax.set_xlim([0, 10])
        ax.set_ylim([0, 10])

        # plt.show()

        plt.draw()
        plt.pause(0.1)

def plot_roomba(x, y, yaw):
    """

    Args:
        x ():
        y ():
        yaw ():
    """
    LENGTH = 0.5  # [m]
    WIDTH = 0.25  # [m]
    OFFSET = LENGTH  # [m]

    fig = plt.gcf()
    ax = fig.gca()
    circle = plt.Circle((x, y), .5, color='b', fill=False)
    ax.add_patch(circle)

    # Plot direction marker
    dx = 1 * np.cos(yaw)
    dy = 1 * np.sin(yaw)
    ax.arrow(x, y, dx, dy, head_width=0.1, head_length=0.1, fc='r', ec='r')

if __name__ == "__main__":

    # Example usage

    file_path = "experiments/settings_files/4robotscircle.yaml"
    import yaml
    with open(file_path, 'r') as file:
        settings = yaml.safe_load(file)

    initial_pos = np.array([0.0, 0.5, 0.0])
    dynamics = Roomba(settings)
    target_vocity = 3.0 # m/s
    T = 5  # Prediction Horizon [s]
    DT = 0.2  # discretization step [s]
    wp = [[0, 1,2,3,4,5,6,7,8,9],
          [0, 0,0,0,0,0,0,0,0,0]]
    sim = PathTracker(initial_position=initial_pos, dynamics=dynamics, target_v=target_vocity, T=T, DT=DT, waypoints=wp, settings=settings)
    x,y,h = sim.run(show_plots=True)

    