import numpy as np
import casadi as ca
from guided_mrmp.controllers.optimizer import Optimizer

np.seterr(divide="ignore", invalid="ignore")

class MultiMPC:
    def __init__(self, num_robots, model, T, DT, settings, circle_obs, rectangle_obs):
        """
        Initializes the MPC controller.
        """
        self.nx = model.state_dimension()  # number of state vars 
        self.nu = model.control_dimension()  # number of input/control vars
        self.num_robots = num_robots
        self.robot_radius = model.radius

        self.robot_model = model
        self.dt = DT

        self.circle_obs = circle_obs
        self.rectangle_obs = rectangle_obs
        self.obstacle_bias = 0.05

        state_cost = settings['model_predictive_controller']['Q']  # state error cost
        final_state_cost = settings['model_predictive_controller']['Qf']  # state final error cost
        input_cost = settings['model_predictive_controller']['R']  # input cost
        input_rate_cost = settings['model_predictive_controller']['P']  # input rate of change cost

        self.bf_c = settings['model_predictive_controller']['barrier_function_c']
        self.bf_max_d = settings['model_predictive_controller']['barrier_function_max_d']

        # how far we can look into the future divided by our dt 
        # is the number of control intervals
        self.control_horizon = int(T / DT) 

        # Weight for the error in state
        self.Q = np.diag(state_cost)

        # Weight for the error in final state
        self.Qf = np.diag(final_state_cost)

        # weight for error in control
        self.R = np.diag(input_cost)
        self.P = np.diag(input_rate_cost)

        # Optimization settings
        self.robot_robot_collision_weight = settings['model_predictive_controller']['robot_robot_collision_weight']
        self.obstacle_collision_weight = settings['model_predictive_controller']['obstacle_collision_weight']
        self.acceptable_tol = settings['model_predictive_controller']['acceptable_tol']
        self.acceptable_iter = settings['model_predictive_controller']['acceptable_iter']
        self.print_level = settings['model_predictive_controller']['print_level']
        self.print_time = settings['model_predictive_controller']['print_time']


    def apply_quadratic_barrier(self, d_max, d, c):
        """
        Applies a quadratic barrier to some given distance. The quadratic barrier 
        is a soft barrier function. We are using it for now to avoid any issues with
        invalid initial solutions, which hard barrier functions cannot handle. 

        params:
            d (float):      distance to the obstacle
            c (float):      controls the steepness of curve. 
                            higher c --> gets more expensive faster as you move toward obs
            d_max (float):  The threshold distance at which the barrier starts to apply 
        """
        return c*ca.fmax(0, (d_max-d)**2)

    def dist_robot_to_rectangle(self, x, y, r, rect):
        """
        Calculate the distance between a robot and a rectangle obstacle.
        x, y: robot position
        r: robot radius
        rect: rectangle obstacle (x0, y0, width, height)
        """

        x0, y0, w, h = rect
        dx = ca.fmax(0, x - x0) + ca.fmax(0, x0 + w - x)
        dy = ca.fmax(0, y - y0) + ca.fmax(0, y0 + h - y)
        return ca.sqrt(dx**2 + dy**2) - r


    def setup_mpc_problem(self, initial_state, target, prev_cmd, As, Bs, Cs):
        """
        Create the cost function and constraints for the optimization problem.

        inputs:
            - initial_state (nx3 array): Initial state for each robot
            - target : Target state for each robot
            - prev_cmd: Previous control input for each robot
            - As: List of A matrices for each robot
            - Bs: List of B matrices for each robot
            - Cs: List of C matrices for each robot
        """

        print(f"Initial state: {initial_state}")

        opti = ca.Opti()

        # Set up state variable
        # X is of the form [[x1_k0, y1_k0, theta1_k0, x2_k0, y2_k0, theta2_k0, ...], [x1_k1, y1_k1, theta1_k1, x2_k1, y2_k1, theta2_k1, ...], ...]
        X = opti.variable(self.nx*self.num_robots, self.control_horizon + 1)
        pos = X[:self.num_robots*2,:]               # position is the first two values
        x = pos[0::2,:]
        y = pos[1::2,:]
        heading = X[self.num_robots*2:,:]           # heading is the last value

        # control variables
        # U is of the form [[v1_k0, omega1_k0, v2_k0, omega2_k0, ...], [v1_k1, omega1_k1, v2_k1, omega2_k1, ...], ...]
        U = opti.variable(self.nu*self.num_robots, self.control_horizon)

        # # parameters, these parameters are the reference trajectories of the pose and inputs
        # opt_u_ref = opti.parameter(N, 2)
        # opt_x_ref = opti.parameter(N+1, 3)

        # Parameters
        # initial_state = ca.MX(initial_state)

        

        # create model
        f = lambda x_, u_: ca.vertcat(*[u_[0]*ca.cos(x_[2]), u_[0]*ca.sin(x_[2]), u_[1]])
        f_np = lambda x_, u_: np.array([u_[0]*np.cos(x_[2]), u_[0]*np.sin(x_[2]), u_[1]])

        # initial condition
        for i in range(self.num_robots):
            opti.subject_to(X[i*3:i*3+3, 0] == initial_state[i])

        # next state constraints
        for i in range(self.control_horizon):
            for j in range(self.num_robots):
                this_X = X[j*3:j*3+3, i]
                this_U = U[j*2:j*2+2, i]
                next_state = X[j*3:j*3+3, i+1]
                x_next = this_X + f(this_X, this_U)*self.dt
                opti.subject_to(next_state == x_next)

        # add constraints to obstacle
        # for i in range(self.control_horizon+1):
        #     for obs in self.circle_obs:
        #         for j in range(self.num_robots):
        #             # print("obs", obs)
        #             temp_constraints_ = ca.sqrt((x[j, i]-obs[0]-self.obstacle_bias)**2+(y[j, i]-obs[1]-self.obstacle_bias)**2)-self.robot_radius-obs[2]
        #             opti.subject_to(opti.bounded(.75, temp_constraints_, 20.0))

        # bounds constraints
        v_max = 20.0
        omega_max = np.pi/2.0
        for i in range(self.num_robots):
            opti.subject_to(opti.bounded(-v_max, U[i*2, :], v_max))
            opti.subject_to(opti.bounded(-omega_max, U[i*2+1, :], omega_max))
            # opti.subject_to(ca.fabs(U[i*2, 0] - prev_cmd[i][0]) / self.dt <= self.robot_model.max_d_vel)
            # opti.subject_to(ca.fabs(U[i*2+1, 0] - prev_cmd[i][1]) / self.dt <= self.robot_model.max_d_steer)

            # for k in range(1, self.control_horizon):
            #     opti.subject_to(ca.fabs(U[i*2, k] - U[i*2, k-1]) / self.dt <= self.robot_model.max_d_vel)
            #     opti.subject_to(ca.fabs(U[i*2+1, k] - U[i*2+1, k-1]) / self.dt <= self.robot_model.max_d_steer)

    

        # Set up cost function
        cost = 0
        for k in range(self.control_horizon):
            for i in range(self.num_robots):
                this_target = [target[i][0][k], target[i][1][k], target[i][2][k]]


                # difference between the current state and the target state
                # cost += ca.mtimes([(X[i*3 : i*3 +3, k+1] - this_target).T, self.Q, X[i*3 : i*3 +3, k+1] - this_target])
            
                state_error = X[i*3 : i*3 +3, k] - this_target
                cost += ca.mtimes([state_error.T, self.Q, state_error])

                # control effort
                cost += ca.mtimes([U[i*2:i*2+2, k].T, self.R, U[i*2:i*2+2, k]])
            # if k > 0:
            #     # Penalize large changes in control
            #     cost += ca.mtimes([(U[i*2:i*2+2, k] - U[i*2:i*2+2, k-1]).T, self.P, U[i*2:i*2+2, k] - U[i*2:i*2+2, k-1]])


        # Final state cost
        # for i in range(self.num_robots):
        #     final_target = this_target = [target[i][0][-1], target[i][1][-1], target[i][2][-1]]
        #     cost += ca.mtimes([(X[i*3 : i*3 +3, -1] - final_target).T, self.Qf, X[i*3 : i*3 +3, -1] - final_target])

        # robot-robot collision cost
        dist_to_other_robots = 0
        for k in range(self.control_horizon):
            for r1 in range(self.num_robots):
                for r2 in range(r1+1, self.num_robots):
                    if r1 != r2:
                        d = ca.sumsqr(pos[2*r1 : 2*r1+1, k] - pos[2*r2 : 2*r2+1, k]) 
                        d = ca.sqrt(d)
                        dist_to_other_robots += self.apply_quadratic_barrier(self.bf_max_d*self.robot_radius, d-self.robot_radius*2, self.bf_c)

        # obstacle collision cost
        obstacle_cost = 0
        # distance = 0
        for k in range(self.control_horizon):
            for i in range(self.num_robots):
                print(f"Robot {i} at time {k}")
                for obs in self.circle_obs:
                    d = ca.sumsqr(x[i, k] - obs[0]) + ca.sumsqr(y[i, k] - obs[1])
                    d = ca.sqrt(d)
                    obstacle_cost += self.apply_quadratic_barrier(self.bf_max_d*(self.robot_radius + obs[2]) , d-self.robot_radius - obs[2], self.bf_c)
                for obs in self.rectangle_obs:
                    d = self.dist_robot_to_rectangle(x[i, k], y[i, k], self.robot_radius, obs)
                    obstacle_cost += self.apply_quadratic_barrier(6*self.robot_radius, d, 1)

        opti.minimize(cost + self.robot_robot_collision_weight*dist_to_other_robots + self.obstacle_collision_weight*obstacle_cost)
       
        # Constraints
        # for i in range(self.num_robots):
        #     for k in range(self.control_horizon):
                # A = ca.MX(As[i])
                # B = ca.MX(Bs[i])
                # C = ca.MX(Cs[i])
                # opti.subject_to(X[i*3:i*3+3, k+1] == ca.mtimes(A, X[i*3:i*3+3, k]) + ca.mtimes(B, U[i*2:i*2+2, k]) + C)

        # pi = [3.14159]*self.num_robots
        # pi = np.array(pi)
        # pi = ca.DM(pi)

        # parameters, these parameters are the reference trajectories of the pose and inputs
        # opt_u_ref = opti.parameter(N, 2)
        # opt_x_ref = opti.parameter(N+1, 3)


        # for k in range(self.control_horizon): # loop over control intervals
        #     dxdt = vel[:,k] * ca.cos(heading[:,k])
        #     dydt = vel[:,k] * ca.sin(heading[:,k])
        #     dthetadt = omega[:,k]
        #     opti.subject_to(x[:,k+1]==x[:,k] + self.dt*dxdt)
        #     opti.subject_to(y[:,k+1]==y[:,k] + self.dt*dydt) 
        #     opti.subject_to(heading[:,k+1]==ca.fmod(heading[:,k] + self.dt*dthetadt, 2*pi))



        return {
            'opti': opti,
            'X': X,
            'U': U,
            'initial_state': initial_state,
            'target': target,
            'prev_cmd': prev_cmd,
            'cost': cost,
            'dist_to_other_robots': dist_to_other_robots,
            'obs_cost': obstacle_cost
        }

    def solve_optimization_problem(self, problem, initial_guesses=None, solver_options=None):
        opt = Optimizer(problem)
        results = opt.solve_optimization_problem(initial_guesses, solver_options)
        return results

    def step(self, initial_state, target, prev_cmd, initial_guesses=None):
        """
        Sets up and solves the optimization problem.
        
        Args:
            initial_state: List of current estimates of [x, y, heading] for each robot
            target: State space reference, in the same frame as the provided current state
            prev_cmd: List of previous commands [v, delta] for all robots  
            initial_guess: Optional initial guess for the optimizer
        
        Returns:
            x_opt: Optimal state trajectory
            u_opt: Optimal control trajectory
        """
        As, Bs, Cs = [], [], []
        for i in range(self.num_robots):
            A, B, C = self.robot_model.linearize(initial_state[i], prev_cmd[i], self.dt)
            As.append(A)
            Bs.append(B)
            Cs.append(C)

        solver_options = {'ipopt.print_level': self.print_level, 
                          'print_time': self.print_time, 
                          'ipopt.acceptable_tol': self.acceptable_tol, 
                          'ipopt.acceptable_iter': self.acceptable_iter}

        problem = self.setup_mpc_problem(initial_state, target, prev_cmd, As, Bs, Cs)

        result = self.solve_optimization_problem(problem, initial_guesses, solver_options)

        if result['status'] == 'succeeded':
            x_opt = result['X']
            u_opt = result['U']
        else:
            print("Optimization failed")
            x_opt = None
            u_opt = None

        return x_opt, u_opt

