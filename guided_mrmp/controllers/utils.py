"""
Various utility functions for the controllers.
"""
from shapely.geometry import Point
from shapely.geometry import Polygon

import numpy as np
from scipy.interpolate import interp1d

def compute_path_from_wp(start_xp, start_yp, step=0.1):
    """
    params:
        start_xp (array-like): 1D array of x-positions
        start_yp (array-like): 1D array of y-positions
        step (float): interpolation step size 

    output:
        ndarray of shape (3,N) representing the  path as x,y,heading
    """
    final_xp = []
    final_yp = []
    delta = step  # [m]
    for idx in range(len(start_xp) - 1):
        # find the distance between consecutive waypoints
        section_len = np.sum(
            np.sqrt(
                np.power(np.diff(start_xp[idx : idx + 2]), 2)
                + np.power(np.diff(start_yp[idx : idx + 2]), 2)
            )
        )

        # how many interpolated points are needed to reach the next waypoint
        interp_range = np.linspace(0, 1, np.floor(section_len / delta).astype(int))  

        # interpolate between waypoints
        fx = interp1d(np.linspace(0, 1, 2), start_xp[idx : idx + 2], kind=1)
        fy = interp1d(np.linspace(0, 1, 2), start_yp[idx : idx + 2], kind=1)

        # append the interpolated points to the final path 
        final_xp = np.append(final_xp, fx(interp_range)[1:])
        final_yp = np.append(final_yp, fy(interp_range)[1:])
    dx = np.append(0, np.diff(final_xp))
    dy = np.append(0, np.diff(final_yp))
    theta = np.arctan2(dy, dx)
    return np.vstack((final_xp, final_yp, theta))

def get_nn_idx(state, path, visited=[]):
    """
    Helper function to find the index of the nearest path point to the current state.

    The "nearest" point is defined as the point with the smallest Euclidean distance 
    to the current state that has not already been visited.

    Args:
        state (array-like): Current state [x, y, theta]
        path (ndarray): Path points [[x1, y1], [x2, y2], ...]
        visited (array-like): Visited path points [[x1, y1], [x2, y2], ...]

    Returns:
        int: Index of the nearest path point
    """
    # Calculate the Euclidean distance between the current state and all path points
    distances = np.linalg.norm(path[:2] - state[:2].reshape(2, 1), axis=0)
    
    # Set the distance to infinity for visited points
    # for point in visited:
    #     point = np.array(point)
    #     # print(f"point = {point}")
    #     # print(f"path[:2] = {path[:2]}")
    #     # Set the distance to infinity for visited points
    #     distances = np.where(np.linalg.norm(path[:2] - point.reshape(2, 1), axis=0) < 1e-3, np.inf, distances)
    
    return np.argmin(distances)

def fix_angle_reference(angle_ref, angle_init):
    """
    Removes jumps greater than 2PI to smooth the heading.

    Args:
        angle_ref (array-like): Reference angles
        angle_init (float): Initial angle

    Returns:
        array-like: Smoothed reference angles
    """
    diff_angle = angle_ref - angle_init
    diff_angle = np.unwrap(diff_angle)
    return angle_init + diff_angle

def get_ref_trajectory(state, path, T, DT, points_visited=[]):
    """
    Generates a reference trajectory for the Roomba.

    Args:
        state (array-like): Current state [x, y, theta]
        path (ndarray): Path points [x, y, theta] in the global frame
        path_visited_points (array-like): Visited path points [[x, y], [x, y], ...]
        target_v (float): Desired speed
        T (float): Control horizon duration
        DT (float): Control horizon time-step

    Returns:
        ndarray: Reference trajectory [x_k, y_k, theta_k] in the ego frame
    """
    K = int(T/DT)

    # Find the last visited point
    last_visited_idx = 0 if points_visited == [] else points_visited[-1]


    next_ind = last_visited_idx + 1
    points_visited.append(next_ind)


    x_ = np.zeros((3, K+1))
    x_[:,0] = state

    for i in range(K):
        if next_ind+i >= len(path[0]):
            x_ref_ = path[0, -1]
            y_ref_ = path[1, -1]
            theta_ref_ = path[2, -1]
        else:
            x_ref_ = path[0, next_ind+i]
            y_ref_ = path[1, next_ind+i]
            theta_ref_ = path[2, next_ind+i]
        x_[:,i+1] = np.array([x_ref_, y_ref_, theta_ref_])

    return x_, points_visited


def ego_to_global_roomba(state, mpc_out):
    """
    Transforms optimized trajectory XY points from ego (robot) reference
    into global frame.

    Args:
        mpc_out (numpy array): Optimized trajectory points in ego reference frame.

    Returns:
        numpy array: Transformed trajectory points in global frame.
    """
    # Extract x, y, and theta from the state
    x = state[0]
    y = state[1]
    theta = state[2]

    # Rotation matrix to transform points from ego frame to global frame
    Rotm = np.array([
        [np.cos(theta), -np.sin(theta)],
        [np.sin(theta), np.cos(theta)]
    ])

    # Initialize the trajectory array (only considering XY points)
    trajectory = mpc_out[0:2, :]

    # Apply rotation to the trajectory points
    trajectory = Rotm.dot(trajectory)

    # Translate the points to the robot's position in the global frame
    trajectory[0, :] += x
    trajectory[1, :] += y

    return trajectory

def get_obstacle_map(grid_origin, grid_size, cell_size, circle_obs):
    """
    Create a map of the environment with obstacles
    """
    # create a grid of size grid_size x grid_size
    grid = np.zeros((grid_size, grid_size))

    # for i in range(grid_size):
    #     for j in range(grid_size):
    #         for obs in circle_obs:
    #             # collision check this square and the obstacle circle using shapely
    #             circle = Point(obs[0], obs[1]).buffer(obs[2])
    #             cell_corners = np.array([(0, 0), (cell_size, 0), (cell_size, cell_size), (0, cell_size)])
    #             cell_corners += np.array(grid_origin)
    #             cell_corners += np.array([i, j]) * cell_size
    #             cell_geom = Polygon(cell_corners)
    #             overlap_area = circle.intersection(cell_geom).area
    #             print(f"overlap = {overlap_area}, threshold = {cell_size**2 / 3}")
    #             if overlap_area >= (cell_size**2 / 3 ):
    #                 grid[i][j] = 1


    for obs in circle_obs: 
        # find the cell with greatest overlap with the circle
        circle = Point(obs[0], obs[1]).buffer(obs[2])
        max_i, max_j = None, None
        max = 0 
        for i in range(grid_size):
            for j in range(grid_size):
                cell_corners = np.array([(0, 0), (cell_size, 0), (cell_size, cell_size), (0, cell_size)])
                cell_corners += np.array(grid_origin)
                cell_corners += np.array([i, j]) * cell_size
                cell_geom = Polygon(cell_corners)
                overlap_area = circle.intersection(cell_geom).area
                if overlap_area >= (cell_size**2 / 3 ):
                    grid[i][j] = 1
                if overlap_area >= max:
                    max = overlap_area
                    max_i = i
                    max_j = j
        max_ij_cell_corners = np.array([(0, 0), (cell_size, 0), (cell_size, cell_size), (0, cell_size)])
        max_ij_cell_corners += np.array(grid_origin)
        max_ij_cell_corners += np.array([max_i, max_j]) * cell_size
        max_ij_cell_geom = Polygon(max_ij_cell_corners)
        overlap_area = circle.intersection(max_ij_cell_geom).area
        if max_i is not None and overlap_area> .1:
            grid[max_i][max_j] = 1
                
            # TODO: add rectangle obstacles

    return grid

def get_grid_cell(x, y, grid_origin, grid_size, cell_size):
    """
    Given a continuous space x and y, find the cell in the grid that includes that location
    """
    import math

    # find the closest grid cell that is not an obstacle
    cell_x = min(max(math.floor((x - grid_origin[0]) / cell_size), 0), grid_size - 1)
    cell_y = min(max(math.floor((y - grid_origin[1]) / cell_size), 0), grid_size - 1)

    return cell_x, cell_y

def get_grid_cell_location(cell_x, cell_y, grid_origin, cell_size):
    """
    Given a cell in the grid, find the center of that cell in continuous space
    """
    x = grid_origin[0] + (cell_x + 0.5) * cell_size
    y = grid_origin[1] + (cell_y + 0.5) * cell_size
    return x, y
   