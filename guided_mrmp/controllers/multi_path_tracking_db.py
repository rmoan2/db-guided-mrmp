import numpy as np

from guided_mrmp.controllers.multi_path_tracking import MultiPathTracker
from guided_mrmp.controllers.utils import compute_path_from_wp, get_ref_trajectory, get_grid_cell_location, get_grid_cell, get_obstacle_map
from guided_mrmp.conflict_resolvers.discrete_resolver import DiscreteResolver
from guided_mrmp.conflict_resolvers.curve_path import smooth_path, calculate_headings
from guided_mrmp.utils.helpers import plan_decoupled_path
from guided_mrmp.controllers.vis_helpers import draw_grid_solution, draw_chosen_subp, draw_temp_goals, draw_grid_world_solution

from guided_mrmp.controllers.multi_mpc import MultiMPC
from guided_mrmp.controllers.place_grid import place_grid

class DiscreteRobot:
    def __init__(self, start, goal, label, outside_grid=False):
        self.start = start
        self.goal = goal
        self.current_position = start
        self.label = label
        self.outside_grid = outside_grid

class MultiPathTrackerDB(MultiPathTracker):
    def get_temp_starts_and_goals(self, state, grid_origin):
        """
        Return the temporary starts and goals of the robots in the grid space. 
        The temporary starts are the current positions of the robots snapped to the grid
        The temporary goals are the points at the end of the robot's predicted trajectory
        """

        # find the minimum and maximum x and y values of the grid
        min_x = grid_origin[0]
        max_x = grid_origin[0] + self.cell_size * self.grid_size
        min_y = grid_origin[1]
        max_y = grid_origin[1] + self.cell_size * self.grid_size

        import math
        temp_starts = []
        for r in range(self.num_robots):
            x, y, theta = state[r]
            if x < min_x or x > max_x or y < min_y or y > max_y:
                temp_starts.append([-1, -1])
            else:
                cell_x = min(max(math.floor((x - grid_origin[0]) / self.cell_size), 0), self.grid_size - 1)
                cell_y = min(max(math.floor((y- grid_origin[1]) / self.cell_size), 0), self.grid_size - 1)
                temp_starts.append([cell_x, cell_y])


        # the temmporary goal is the point at the end of the robot's predicted traj
        temp_goals = []
        for r in range(self.num_robots):
            # traj = self.ego_to_global_roomba(state[r], self.trajs[r])
            traj = self.trajs[r]
            x = traj[0][-1]
            y = traj[1][-1]
            cell_x = min(max(math.floor((x - grid_origin[0]) / self.cell_size), 0), self.grid_size - 1)
            cell_y = min(max(math.floor((y- grid_origin[1]) / self.cell_size), 0), self.grid_size - 1)
            temp_goals.append([cell_x,cell_y])

        return temp_starts, temp_goals
    
    def get_subgoals(self, state, robots_in_conflict):
        subgoals = []
        for idx in robots_in_conflict:
            # traj = self.ego_to_global_roomba(state[idx], self.trajs[idx])
            traj = self.trajs[idx]
            x = traj[0][-1]
            y = traj[1][-1]
            subgoals.append((x, y))
        return subgoals     

    def create_discrete_robots(self, starts, goals):
        discrete_robots = []
        for i in range(len(starts)):
            if starts[i][0] == -1:
                start = starts[i]
                goal = goals[i]
                discrete_robots.append(DiscreteRobot(start, goal, i, outside_grid=True))
            else: 
                start = starts[i]
                goal = goals[i]
                discrete_robots.append(DiscreteRobot(start, goal, i, outside_grid=False))
        return discrete_robots
      
    def get_discrete_solution(self, state, conflict, all_conflicts, grid, grid_origin, libs):
        """
        Inputs:
            - conflict (list): list of robot idxs involved in the conflict
            - all_conflicts (list): list of all conflicts
            - grid (bool array): the obstacle map of grid that we placed
            - grid_origin (tuple): the top left corner of the grid in continuous space
        """

        starts, goals = self.get_temp_starts_and_goals(state, grid_origin)

        # get the starts and goals of ONLY the robots in conflict
        conflict_starts = [starts[c] for c in conflict]
        conflict_goals = [goals[c] for c in conflict]

        # create a "discrete" version of the robot, i.e. the 
        # format that the discrete solver expects to receive 
        disc_robots = self.create_discrete_robots(starts, goals)

        disc_conflict = []
        for r in disc_robots:
            if r.label in conflict:
                disc_conflict.append(r)

        disc_all_conflicts = []
        for c in all_conflicts:
            this_conflict = []
            for r in disc_robots:
                if r.label in c:
                    this_conflict.append(r)
            disc_all_conflicts.append(this_conflict)

        grid_solver1 = DiscreteResolver(disc_conflict, disc_robots, conflict_starts, conflict_goals, disc_all_conflicts,grid, libs[0], libs[1], libs[2])
        subproblem1 = grid_solver1.find_subproblem()

        if subproblem1 is None:
            print("Couldn't find a discrete subproblem")
            draw_grid_solution(self.num_robots, self.radius, self.grid_size, self.cell_size, self.env, self.trajs, [], state, grid_origin, grid, conflict, None, None, [])
            return None
        
        print(f"old conflict goals = {conflict_goals}")
        print(f"discrete solver goals = {grid_solver1.goals}")
        print(f"Subproblem1 goals = {subproblem1.get_goals()}")
        print(f"subproblem1 temp goals = {subproblem1.temp_goals}")

        # reassign the temp goals in the subproblem by modify subproblem.temp_goals
        # the new temp goals should be the point furthest along the robot's guide path that is still 
        # contatined inside the subproblem, then snapped to the grid
        top_left = subproblem1.top_left
        bottom_right = subproblem1.bottom_right
        max_x = grid_origin[0] + (bottom_right[0]+1)*self.cell_size 
        min_x = grid_origin[0] + top_left[0]*self.cell_size
        min_y = grid_origin[1] + top_left[1]*self.cell_size
        max_y = grid_origin[1] + (bottom_right[1]+1)*self.cell_size

        print(f"min_x = {min_x}, max_x = {max_x}, min_y = {min_y}, max_y = {max_y}")


        temp_goals = []
        for i in range(self.num_robots):
            guide_path = self.paths[i]
            # for point in guide_path:
            xs = guide_path[0]
            ys = guide_path[1]
            
            # iterate over x,y values in reverse
            for x, y in zip(reversed(xs), reversed(ys)):
                # find the first point that is inside the subproblem
                if x >= min_x and x <= max_x and y >= min_y and y <= max_y:
                    cell_x, cell_y = get_grid_cell(x, y, grid_origin, self.grid_size, self.cell_size)
                    temp_goals.append([cell_x, cell_y])
                    break

        
        new_conflict_goals = [temp_goals[c] for c in conflict]

        # create a "discrete" version of the robot, i.e. the 
        # format that the discrete solver expects to receive 
        disc_robots = self.create_discrete_robots(starts, temp_goals)

        disc_conflict = []
        for r in disc_robots:
            if r.label in conflict:
                disc_conflict.append(r)

        disc_all_conflicts = []
        for c in all_conflicts:
            this_conflict = []
            for r in disc_robots:
                if r.label in c:
                    this_conflict.append(r)
            disc_all_conflicts.append(this_conflict)

        temp_goals_continuous = [get_grid_cell_location(temp_goals[c][0], temp_goals[c][1], grid_origin, self.cell_size) for c in conflict]

        grid_solver2 = DiscreteResolver(disc_conflict, disc_robots, conflict_starts, new_conflict_goals, disc_all_conflicts,grid, libs[0], libs[1], libs[2])
        subproblem2 = grid_solver2.find_subproblem()
        
        print(f"New Conflict goals = {new_conflict_goals}")
        print(f"discrete solver goals = {grid_solver2.goals}")
        print(f"Subproblem goals = {subproblem2.get_goals()}")
        print(f"subproblem temp goals = {subproblem2.temp_goals}")

        

        top_left = subproblem2.top_left
        bottom_right = subproblem2.bottom_right
        max_x = grid_origin[0] + (bottom_right[0]+1)*self.cell_size 
        min_x = grid_origin[0] + top_left[0]*self.cell_size
        min_y = grid_origin[1] + top_left[1]*self.cell_size
        max_y = grid_origin[1] + (bottom_right[1]+1)*self.cell_size

        draw_chosen_subp(self.num_robots, self.radius, self.grid_size, self.cell_size, self.env, self.trajs, state, grid_origin, grid, conflict, (min_x, min_y), (max_x, max_y))
        draw_temp_goals(self.num_robots, self.radius, self.grid_size, self.cell_size, self.env, self.trajs, state, grid_origin, grid, conflict, (min_x, min_y), (max_x, max_y), temp_goals_continuous, self.paths)
        
        if subproblem2 is None:
            print("Couldn't find a discrete subproblem")
            draw_grid_solution(self.num_robots, self.radius, self.grid_size, self.cell_size, self.env, self.trajs, [], state, grid_origin, grid, conflict)
            return None
        grid_solution = grid_solver2.solve_subproblem(subproblem2)


        if grid_solution is None:
            draw_grid_solution([], state, grid_origin, grid, conflict, 0)

        # The solution for each robot must be of the same length, so the arrays are padded with [-1,-1]
        # so they are of the form [[x, y], [x, y], ..., [-1,1], [-1,1], ...]
        # Clean up the grid solution so that it doesn't contain any -1s

        unpadded_grid_solution = []
        for sol in grid_solution:
            new_sol = []
            for point in sol:
                if point[0] == -1: break
                new_sol.append(point)
            unpadded_grid_solution.append(new_sol)

        print(f"Grid Solution = {unpadded_grid_solution}")
        draw_grid_world_solution(self.num_robots, self.radius, self.grid_size, self.cell_size, self.env, self.trajs, [], state, grid_origin, grid, c, (min_x, min_y), (max_x, max_y), unpadded_grid_solution)


        return unpadded_grid_solution
    
    def convert_db_sol_to_continuous(self, state, grid_solution, grid_origin, robots_in_conflict, N, cp_dist, smoothing=0.0):
        """
        Takes a solution from the discrete solver and converts it to a continuous space solution
        The points on the grid solution are treated as points on a bezier curve. 

        params:
            - state (list): the current state of the robots [[x1, y1, theta1], [x2, y2, theta2], ...]
            - grid_solution (list): the solution from the discrete solver [[(x1, y1), (x2, y2), ...], [(x1, y1), (x2, y2), ...], ...]
            - robots_in_conflict (list): the indices of the robots in conflict
            - dt (float): the discretization step size 
            - N (int): the number of points to interpolate between each pair of points on the bezier curve
            - cp_dist (float): how far away the cps of the bezier curve are

        returns:
            - estimated_state (np.ndarray): the estimated state of the robots in continuous space
        """
        num_robots = len(robots_in_conflict)
        estimated_state = np.zeros((num_robots*3, N+1))
        final_estimated_state = np.zeros((num_robots*3, 15+N+1))


        for i in range(num_robots):
            points = np.array(grid_solution[i])
            
            # smooth the path using bezier curves and gaussian smoothing
            smoothed_curve, _ = smooth_path(points, N+1, cp_dist, smoothing)
            estimated_state[i*3, :] = smoothed_curve[:, 0]        # x
            estimated_state[i*3 + 1, :] = smoothed_curve[:, 1]    # y
            # import matplotlib.pyplot as plt
            # plt.plot(estimated_state[i*3, :], estimated_state[i*3 + 1, :], 'b-', label="Bezier Curve")
            # plt.plot(points[:, 0], points[:, 1], 'ro', label="Control Points")
            # plt.legend()
            # plt.show()

            # translate the initial guess so that the first point is at (0,0)
            estimated_state[i*3, :] -= estimated_state[i*3, 1]
            estimated_state[i*3 + 1, :] -= estimated_state[i*3+1, 1]

            estimated_state[i*3, :] *= self.cell_size

            cell_loc = get_grid_cell_location(points[0][0], points[0][1], grid_origin, self.cell_size)

            # translate the initial guess so that the first point is at the first point of the continuous space soln
            estimated_state[i*3, :] += cell_loc[0]
            estimated_state[i*3 + 1, :] += cell_loc[1] 
            
            headings = calculate_headings(smoothed_curve)
            headings.append(headings[-1])

            estimated_state[i*3 + 2, :] = headings

            

            # vector a should be the vector from the cell center to the robot position
            current_robot_position = state[robots_in_conflict[i]][:2]
            current_robot_position = np.array(current_robot_position)
            cell_loc = np.array(cell_loc)
            a_vector = current_robot_position - cell_loc

            # vector b should be the vector from the cell center to the 4th point of the estimated state
            b_vector = np.array([estimated_state[i*3, 3], estimated_state[i*3 + 1, 3]]) - cell_loc

            # project a onto b
            a_dot_b = np.dot(a_vector, b_vector)
            b_dot_b = np.dot(b_vector, b_vector)
            proj = a_dot_b / b_dot_b * b_vector

            cp2 = proj

            cp1 = a_vector + cell_loc
            cp2 = proj + cell_loc
            cp3 = b_vector + cell_loc


            # plot the control points, the cell center, and the robot position, b vecotr, and a vector
            import matplotlib.pyplot as plt

            plt.plot([cp1[0], cp2[0], cp3[0]], [cp1[1], cp2[1], cp3[1]], 'ro', label="control points")
            plt.plot(cell_loc[0], cell_loc[1], 'bo', label="cell center")
            plt.plot(current_robot_position[0], current_robot_position[1], 'go', label="robot position")
            plt.plot(b_vector[0], b_vector[1], 'yo', label="b vector")
            plt.plot(a_vector[0], a_vector[1], 'yo', label="a vector")
            plt.plot(proj[0], proj[1], 'yx', label="proj")
            plt.legend()
            plt.show()


            # create a smooth bezier curve using these control points
            smoothed_curve, _ = smooth_path([cp1, cp2, cp3], 10+1, 0, 0)
            
            xs = smoothed_curve[:, 0]
            ys = smoothed_curve[:, 1]
            headings = calculate_headings(smoothed_curve)
            headings.append(headings[-1])

            # now add a few points to the beginning of the curve to orient the heading
            current_robot_heading = state[robots_in_conflict[i]][2]
            goal_heading = np.arctan2(ys[1] - ys[0], xs[1] - xs[0])

            diff = (goal_heading - current_robot_heading) % (2*np.pi)

            # if diff < 0: diff += 2*np.pi

            delta_heading = diff / 4

            for j in range(4):
                xs = np.insert(xs, 0, xs[0])
                ys = np.insert(ys, 0, ys[0])
                headings.insert(0, current_robot_heading + delta_heading*(j+1))
                current_robot_heading += delta_heading


            # plot the bezier curve, along with the control points
            # plt.plot(xs, ys, 'b-', label="Bezier Curve")
            # plt.plot([cp1[0], cp2[0], cp3[0]], [cp1[1], cp2[1], cp3[1]], 'ro', label="control points")
            # plt.legend()
            # plt.show()


            # # plot the estimated state
            # plt.plot(estimated_state[i*3, :], estimated_state[i*3 + 1, :], 'b-', label="Bezier Curve")

            
            # Finally, combine the interpolated points with the estimated state
            final_estimated_state[i*3, :] = np.concatenate((xs, estimated_state[i*3, :]))
            final_estimated_state[i*3 + 1, :] = np.concatenate((ys, estimated_state[i*3 + 1, :]))
            final_estimated_state[i*3 + 2, :] = np.concatenate((headings, estimated_state[i*3 + 2, :]))

        return final_estimated_state

    def get_next_state_control(self, state, u_next, x_next):
        targets = []
        for i in range(self.num_robots):
            ref, visited_guide_points = get_ref_trajectory(np.array(state[i]), 
                                                           np.array(self.paths[i]),
                                                           self.T, 
                                                           self.DT, 
                                                           self.visited_points_on_guide_paths[i])
            
            self.visited_points_on_guide_paths[i] = visited_guide_points

            targets.append(ref)
        
            mpc = MultiMPC(1, # num robots
                           self.dynamics, 
                           self.T, 
                           self.DT, 
                           self.settings, 
                           self.env.circle_obs, 
                           self.env.rect_obs)
            
            # curr_state = np.zeros((1, 3))
            curr_state = np.array([state[i]])
            x_mpc, u_mpc = mpc.step(
                curr_state,
                [ref],
                [self.control[i]]
            )

            # x_mpc_global = self.ego_to_global_roomba(state[i], x_mpc)
            u_next[i] = [u_mpc[0, 0], u_mpc[1, 0]]
            x_next[i] = x_mpc

        self.trajs = targets 
        return x_next, u_next
    
    def get_next_state_control_parallel(self, states, u_next, x_next):

        from joblib import Parallel, delayed
        
        def process_robot(idx):
            state = states[idx]
            path = self.paths[idx]
            current_control = self.control[idx]

            # Get Reference_traj -> inputs are in worldframe
            ref, visited_guide_points = get_ref_trajectory(np.array(state), 
                                            np.array(path), 
                                            self.target_v, 
                                            self.T, 
                                            self.DT,
                                            [])
            
            
            mpc = MultiMPC(1, # num robots
                           self.dynamics, 
                           self.T, 
                           self.DT, 
                           self.settings, 
                           self.env.circle_obs, 
                           self.env.rect_obs)
            
            # curr_state = np.zeros((1, 3))
            curr_state = np.array([state])
            x_mpc, u_mpc = mpc.step(
                curr_state,
                [ref],
                [self.control[idx]]
            )

                
            # only the first one is used to advance the simulation
            control = [u_mpc[0, 0], u_mpc[1, 0]]

            return np.asarray(control), x_mpc, ref

        with Parallel(n_jobs=4, backend="loky") as parallel:
            results = parallel(delayed(process_robot)(idx) for idx in range(self.num_robots))

        next_controls = [r[0] for r in results]
        next_trajs = [r[1] for r in results]
        refs = [r[2] for r in results]
        self.trajs = refs

        return next_trajs, next_controls

    def reroute_guide_paths(self, new_ref, old_paths, robot_idx, i ):
        # plan from the last point of the ref path to the robot's goal
        # plan an RRT path from the current state to the goal
        start = (new_ref[0][-1], new_ref[1][-1])
        goal = (old_paths[i][0][-1], old_paths[i][1][-1])

        rrtpath, tree = plan_decoupled_path(self.settings["sampling_based_planner"]["name"], 
                                            start, 
                                            goal, 
                                            self.env, 
                                            self.radius, 
                                            self.env.circle_obs, 
                                            self.env.rect_obs, 
                                            vis=False, 
                                            iter=self.settings["sampling_based_planner"]["num_samples"],
                                            obstacle_tol=0)

        xs = []
        ys = []

        for node in rrtpath:
            xs.append(node[0])
            ys.append(node[1])

        wp = [xs,ys]

        # Path from waypoint interpolation
        path = compute_path_from_wp(wp[0], wp[1], 0.2)

        # combine the path with new_ref
        new_ref_x = np.concatenate((new_ref[0, :], path[0]))
        new_ref_y = np.concatenate((new_ref[1, :], path[1]))
        new_ref_theta = np.concatenate((new_ref[2, :], path[2]))

        self.paths[i] = np.array([new_ref_x, new_ref_y, new_ref_theta])

        self.visited_points_on_guide_paths[i] = []
        
    def advance(self, state, time, libs, show_plots=False):

        waiting = False
        all_waiting_robots = []

        u_next = [[] for _ in range(self.num_robots)]
        x_next = [[] for _ in range(self.num_robots)]

        # 1. Get the mpc trajectory for each robot
        x_next, u_next = self.get_next_state_control(state, u_next, x_next)
        
        # 2. Check if the targets of any two robots overlap
        all_conflicts = []
        for i in range(self.num_robots):
            # traj1 = self.ego_to_global_roomba(state[i], self.trajs[i])
            traj1 = self.trajs[i]
            this_robot_conflicts = [i]
            for j in range(i + 1, self.num_robots):
                # traj2 = self.ego_to_global_roomba(state[j], self.trajs[j])
                traj2 = self.trajs[j]
                if self.trajectories_overlap(traj1, traj2, self.radius):
                    this_robot_conflicts.append(j)
            if len(this_robot_conflicts) > 1:
                all_conflicts.append(this_robot_conflicts)

        for c in all_conflicts:
            # plot the conflict:
            # draw_conflict(self.num_robots, self.radius, self.env, self.trajs, state, c)

            # 3. If they do collide, then reroute the reference trajectories of these robots
            # Get the positions of robots involved in the conflict
            robot_positions = [ state[i][:2] for i in c ]

            # find the  diff in y values of the robots in conflict
            y_diff = robot_positions[0][1] - robot_positions[1][1]

            

            # Put down a local grid
            self.cell_size = self.radius*2
            self.grid_size = 6

            if y_diff > self.cell_size*4:
                continue

            labeled_circle_obs = []
            for obs in self.circle_obs:
                labeled_circle_obs.append(('c',obs[0], obs[1], obs[2]))

            subgoals = self.get_subgoals(state, c)

            grid_origin, centers = place_grid(robot_positions, 
                                                cell_size=self.cell_size, 
                                                grid_size=self.grid_size, 
                                                subgoals=subgoals,
                                                obstacles=labeled_circle_obs)
            grid_obstacle_map = get_obstacle_map(grid_origin, self.grid_size, self.cell_size, self.circle_obs) 

            # draw the grid that was placed
            draw_grid_solution(self.num_robots, self.radius, self.grid_size, self.cell_size, self.env, self.trajs, [], state, grid_origin, grid_obstacle_map, c, None, None, [])

            # Solve a discrete version of the problem 
            # Find a subproblem and solve it
            grid_solution = self.get_discrete_solution(state, c, all_conflicts, grid_obstacle_map, grid_origin, libs)

            # if there is solution that has a sequence of nodes that look like: 
            # [x,y], [x+1,y], [x,y] or [x,y], [x-1,y], [x,y]
            # change it to just be [x,y], [x,y], [x,y]
            if grid_solution:
                for i in range(len(grid_solution)):
                    for j in range(len(grid_solution[i])-2):
                        if grid_solution[i][j] == grid_solution[i][j+2]:
                            grid_solution[i][j+1] = grid_solution[i][j]



            if grid_solution:
                # if we found a grid solution, we can use it to reroute the robots
                continuous_soln = self.convert_db_sol_to_continuous(state, 
                                                                    grid_solution,
                                                                    grid_origin, 
                                                                    c,
                                                                    self.settings["database"]["num_intervals"],
                                                                    self.settings["database"]["control_point_dist"], 
                                                                    self.settings["database"]["smoothing_sigma"]
                                                                    )
                
                draw_grid_solution(self.num_robots, self.radius, self.grid_size, self.cell_size, self.env, self.trajs, continuous_soln, state, grid_origin, grid_obstacle_map, c, None, None, grid_solution)

                # for each robot in conflict, reroute its reference trajectory to match the grid solution
                import copy
                old_paths = copy.deepcopy(self.paths)

                for robot_idx, i in enumerate(c):
                    new_ref = continuous_soln[robot_idx*3:robot_idx*3+3, :]
                    self.reroute_guide_paths(new_ref, old_paths, robot_idx, i)

                # Now replan the reference trajectories to reflect the new guide paths
                for i in c:
                    ref, visited_guide_points = get_ref_trajectory(np.array(state[i]), 
                                                        np.array(self.paths[i]),
                                                        self.T, 
                                                        self.DT, 
                                                        self.visited_points_on_guide_paths[i])
            
                    self.visited_points_on_guide_paths[i] = visited_guide_points
                    self.trajs[i] = ref

                # use MPC to track the new reference trajectories
                # include all the robots that were in conflict in the MPC problem
                # TODO: might instead want to include all robots within a certain distance
                mpc = MultiMPC(len(c), # num robots
                    self.dynamics, 
                    self.T, 
                    self.DT, 
                    self.settings, 
                    self.env.circle_obs, 
                    self.env.rect_obs)

                # curr_states = np.zeros((len(c), 3))
                curr_states = np.array([state[i] for i in c])
                these_trajs = [self.trajs[i] for i in c]
                these_controls = [self.control[i] for i in c]
                x_mpc, u_mpc = mpc.step(
                    curr_states,
                    these_trajs,
                    these_controls
                )

                for i, r in enumerate(c):
                    u_next[r] = [u_mpc[i*2, 0], u_mpc[i*2+1, 0]]
                    x_next[r] = [x_mpc[i*3, 1], x_mpc[i*3+1, 1], x_mpc[i*3+2, 1]]

            else: # Discrete solver failed, resolve the conflict another way
                if waiting: 
                    # Choose all but one robot to "wait" (i.e. controls are 0)
                    c_wait = np.random.choice(c, len(c)-1, replace=False)
                    all_waiting_robots.extend(c_wait)

                else:
                    print("Using coupled solver to resolve conflict")
                    mpc = MultiMPC(self.num_robots, # num robots
                        self.dynamics, 
                        self.T, 
                        self.DT, 
                        self.settings, 
                        self.env.circle_obs, 
                        self.env.rect_obs)

                    curr_states = np.zeros((self.num_robots, 3))
                    for i in range(self.num_robots):
                        curr_states[i] = state[i]
                    x_mpc, u_mpc = mpc.step(
                        curr_states,
                        self.trajs,
                        self.control
                    )

                    # for each robot in conflict, reroute its reference trajectory to match the grid solution
                    import copy
                    old_paths = copy.deepcopy(self.paths)
                    for i in c:
                        u_next[i] = [u_mpc[i*2, 0], u_mpc[i*2+1, 0]]
                        x_next[i] = [x_mpc[i*3, 1], x_mpc[i*3+1, 1], x_mpc[i*3+2, 1]]

                        # update the guide paths of the robots to reflect the new state
                        # new_state = self.ego_to_global_roomba(state[i], x_mpc)
                        new_x = x_mpc[i*3, :]
                        new_y = x_mpc[i*3+1, :]
                        new_theta = x_mpc[i*3+2, :]
                        new_ref = np.array([new_x, new_y, new_theta])

                        self.reroute_guide_paths(new_ref, old_paths, i, i)

        if waiting:
            all_waiting_robots = self.find_all_waiting_robots(all_waiting_robots, state, x_next)
            for i in all_waiting_robots:
                u_next[i] = [0, 0]
                x_next[i] = state[i]

        self.control = u_next

        return x_next, u_next

    def find_all_waiting_robots(self, waiting, state, x_mpc):
        """
        Given a list of robots that have been marked as waiting, find all other robots
        whose next state intersects with the waiting robots' current state. Those robots
        should also be marked as waiting.

        params:
            - waiting (list): list of robots that have already been marked as waiting
            - x_mpcs (list): list of the next states of all robots
        """

        # Init queue with all pairs of (i, j) where i is non-waiting and j is waiting
        queue = [(i, j) for j in waiting for i in range(self.num_robots) if i not in waiting]
        while queue:
            i, j = queue.pop(0)
            if i in waiting: continue # Case where i was originally non-waiting but has since been marked as waiting
            
            i_next_state = x_mpc[i][:2][1]
            j_curr_state = state[j][:2]
            if np.linalg.norm(i_next_state - j_curr_state) < 2*self.radius:
                waiting.append(i)
                # Set i to waiting and add all pairs of (i, k) to the queue for non-waiting k
                for k in range(self.num_robots):
                    if k not in waiting:
                        queue.append((k, i))

        return waiting

