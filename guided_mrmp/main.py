import yaml
import os
import random
import numpy as np
import time
from guided_mrmp.utils import Env, Roomba, Robot, create_random_starts_and_goals
from guided_mrmp.utils.helpers import plan_decoupled_path

from guided_mrmp.simulator import Simulator
from guided_mrmp.utils.helpers import initialize_libraries
from guided_mrmp.controllers import MultiPathTracker, MultiPathTrackerDB

class_function_names_dict = {
    'Roomba': Roomba
    # 'RRT': RRT(),
    # 'RRTStar': RRTStar(),
    }

# robot = class_names_dist[config["robot_name"]](**config["robot_params"])

def load_settings(file_path):
    with open(file_path, 'r') as file:
        settings = yaml.safe_load(file)
    return settings

def set_python_seed(seed):
    print(f"***Setting Python Seed {seed}***")
    os.environ['PYTHONHASHSEED'] = str(seed)
    np.random.seed(seed)
    random.seed(seed)

def initialize_robots(starts, goals, dynamics_models, radii, target_v, env, settings):
    robots = []

    colors = [list(np.random.choice(range(256), size=3)) for i in range(len(starts))]

    for i, (start, goal, dynamics_model, radius, color) in enumerate(zip(starts, goals, dynamics_models, radii, colors)):
        rrtpath, tree = plan_decoupled_path(settings["sampling_based_planner"]["name"], 
                                                  (start[0],start[1]), 
                                                  (goal[0],goal[1]), 
                                                  env, 
                                                  radius, 
                                                  env.circle_obs, 
                                                  env.rect_obs, 
                                                  vis=False, 
                                                  iter=settings["sampling_based_planner"]["num_samples"])
        
        xs = []
        ys = []
        for node in rrtpath:
            xs.append(node[0])
            ys.append(node[1])

        waypoints = [xs,ys]

        start_heading = np.arctan2(ys[1] - start[1], xs[1] - start[0])
        start = [start[0], start[1], start_heading]
        
        r = Robot(i,color,radius,start,goal,dynamics_model,target_v,rrtpath,waypoints, tree)
        robots.append(r)

    return robots

if __name__ == "__main__":

    import argparse
    
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--settings_file", 
        type=str, 
        default="settings_files/settings.yaml"
    )
    parser.add_argument(
        "--env_file", 
        type=str, 
        default="settings_files/env.yaml"
    )
    parser.add_argument(
        "--seed", 
        type=int, 
        default=None
    )
    parser.add_argument(
        "--dont_load_libs", 
        action='store_true'
    )
    args = parser.parse_args()
    
    if args.seed:
        set_python_seed(args.seed)

    # Load the settings
    settings = load_settings(args.settings_file)
    environment = load_settings(args.env_file)

    # Load and create the environment
    circle_obstacles = environment['circle_obstacles']
    rectangle_obstacles = environment['rectangle_obstacles']
    x_range = environment['x_range']
    y_range = environment['y_range']
    env = Env(x_range, y_range, circle_obstacles, rectangle_obstacles)

    # Load the dynamics models
    dynamics_models_st = settings['dynamics_models']
    dynamics_models = []
    for model in dynamics_models_st:
        dynamics_models.append(class_function_names_dict[model](settings))

    # Load and create the robots
    robot_starts = settings['robot_starts']
    robot_goals = settings['robot_goals']
    robot_radii = settings['robot_radii']
    target_v = settings['target_v']
    if robot_starts == []: # if no starts and goals are provided, create random ones
        robot_starts, robot_goals = create_random_starts_and_goals(env, len(robot_radii))
    robots = initialize_robots(robot_starts, robot_goals, dynamics_models, robot_radii, target_v, env, settings)

    # Load the libraries
    if args.dont_load_libs: libs = [None, None, None]
    else: libs = initialize_libraries()

    # Create the Guided MRMP policy
    T = settings['prediction_horizon']
    DT = settings['discretization_step']

    rrt_trees = [robot.tree for robot in robots]
    policy = MultiPathTrackerDB(env=env,
                                 initial_positions=robot_starts, 
                                 dynamics=dynamics_models[0], # NOTE: Using the same dynamics model for all robots for now
                                 target_v=target_v, 
                                 T=T, 
                                 DT=DT, 
                                 waypoints=[robot.waypoints for robot in robots], 
                                 trees = rrt_trees,
                                 settings=settings
                                )

    # Create the simulator
    show_vis = settings['simulator']['show_plots']
    sim = Simulator(robots, dynamics_models, env, policy, settings)

    # Run the simulation
    start = time.time()
    x_hist, y_hist, h_hist = sim.run(libs, show_vis)
    end = time.time()
    print(f"Simulation took {end-start} seconds")

