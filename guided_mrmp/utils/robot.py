import numpy as np

class Robot:
    def __init__(self, label, color, radius, start, goal, dynamics_model, target_v, rrtpath,waypoints, tree):
        self.label = label
        self.color = color
        self.radius = radius
        self.start = start
        self.goal = goal
        self.current_position = start
        self.control = np.zeros(2)
        self.rrtpath = rrtpath
        self.dynamics_model = dynamics_model
        self.waypoints = waypoints
        self.tree = tree

        self.last_mpc_trajectory = None
        self.last_mpc_controls = None
        
        self.next_step = None
        self.next_control = None
        self.target_v = target_v

        self.x_history = [start[0]]
        self.y_history = [start[1]]
        self.h_history = [start[2]]

    def advance(self, map):
        pass