import numpy as np
import math

class Node:
    def __init__(self, current, parent=None, g=0, h=0):
        self.current = current
        self.parent = parent
        self.g = g
        self.h = h
    
    def __add__(self, node):
        return Node((self.x + node.x, self.y + node.y), self.parent, self.g + node.g, self.h)

    def __eq__(self, node) -> bool:
        return self.current == node.current
    
    def __ne__(self, node) -> bool:
        return not self.__eq__(node)

    def __lt__(self, node) -> bool:
        return self.g + self.h < node.g + node.h or \
                (self.g + self.h == node.g + node.h and self.h < node.h)

    def __hash__(self) -> int:
        return hash(self.current)

    def __str__(self) -> str:
        return "----------\ncurrent:{}\nparent:{}\ng:{}\nh:{}\n----------" \
            .format(self.current, self.parent, self.g, self.h)
    
    @property
    def x(self) -> float:
        return self.current[0]
    
    @property
    def y(self) -> float:
        return self.current[1]

    @property
    def px(self) -> float:
        if self.parent:
            return self.parent[0]
        else:
            return None

    @property
    def py(self) -> float:
        if self.parent:
            return self.parent[1]
        else:
            return None

class Env:
    """
    Class for continuous 2-d map.
    """
    def __init__(self, x_range, y_range, circle_obs=[], rectangle_obs=[]):
        self.boundary = [x_range, y_range]

        self.delta = 0.5
        self.circle_obs = circle_obs
        self.rect_obs = rectangle_obs


    def get_obs_vertex(self):
        delta = self.delta
        obs_list = []

        for (ox, oy, w, h) in self.rect_obs:
            vertex_list = [[ox - delta, oy - delta],
                           [ox + w + delta, oy - delta],
                           [ox + w + delta, oy + h + delta],
                           [ox - delta, oy + h + delta]]
            obs_list.append(vertex_list)

        return obs_list

    def get_ray(self,start, end):
        orig = [start.x, start.y]
        direc = [end.x - start.x, end.y - start.y]
        return orig, direc

    def get_dist(self, start, end):
        return math.hypot(end.x - start.x, end.y - start.y)

    def is_intersect_rec(self, start, end, o, d, a, b):
        v1 = [o[0] - a[0], o[1] - a[1]]
        v2 = [b[0] - a[0], b[1] - a[1]]
        v3 = [-d[1], d[0]]

        div = np.dot(v2, v3)

        if div == 0:
            return False

        t1 = np.linalg.norm(np.cross(v2, v1)) / div
        t2 = np.dot(v1, v3) / div

        if t1 >= 0 and 0 <= t2 <= 1:
            shot = Node((o[0] + t1 * d[0], o[1] + t1 * d[1]))
            dist_obs = self.get_dist(start, shot)
            dist_seg = self.get_dist(start, end)
            if dist_obs <= dist_seg:
                return True

        return False

    def is_intersect_circle(self, o, d, a, r):
        d2 = np.dot(d, d)
        delta = self.delta

        if d2 == 0:
            return False

        t = np.dot([a[0] - o[0], a[1] - o[1]], d) / d2

        if 0 <= t <= 1:
            shot = Node((o[0] + t * d[0], o[1] + t * d[1]))
            if self.get_dist(shot, Node(a)) <= r + delta:
                return True

        return False
  
    def is_inside_obs(self, node):
        delta = self.delta

        for (x, y, r) in self.circle_obs:
            if math.hypot(node.x - x, node.y - y) <= r + delta:
                # print("circle collision")
                return True

        for (x, y, w, h) in self.rect_obs:
            if 0 <= node.x - (x - delta) <= w + 2 * delta \
                    and 0 <= node.y - (y - delta) <= h + 2 * delta:
                # print("rect collision")
                return True

        # for (x, y, w, h) in self.obs_boundary:
        #     if 0 <= node.x - (x - delta) <= w + 2 * delta \
        #             and 0 <= node.y - (y - delta) <= h + 2 * delta:
        #         print(f"{node.x}, {node.y} has boundary collision")
        #         return True
            
        if node.x < self.boundary[0][0] or node.x > self.boundary[0][1] or node.y < self.boundary[1][0] or node.y > self.boundary[1][1]:
            return True

        return False
    