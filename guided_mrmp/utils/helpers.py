import random
from guided_mrmp.utils.klampt_planner import CircleRectangleObstacleCSpace, CSpaceObstacleProgram, Circle, Rectangle

"""
Helper Functions that are commonly used in the guided_mrmp package
"""
def create_random_starts_and_goals(env, num_agents):
    """
    Given some map file, create a set of starts and goals for the specified 
    number of agents. This function will create a file folowing the agent file format.
    """

    starts = []
    goals = []

    for i in range(num_agents):
        # generate a random start
        x = random.uniform(env.boundary[0][0], env.boundary[0][1])
        y = random.uniform(env.boundary[1][0], env.boundary[1][1])
    
        while [x,y] in starts:
            x = random.uniform(env.boundary[0][0], env.boundary[0][1])
            y = random.uniform(env.boundary[1][0], env.boundary[1][1])

        starts.append([x,y,0])

        # generate a random goal
        x = random.uniform(env.boundary[0][0], env.boundary[0][1])
        y = random.uniform(env.boundary[1][0], env.boundary[1][1])
    
        while [x,y] in goals:
            x = random.uniform(env.boundary[0][0], env.boundary[0][1])
            y = random.uniform(env.boundary[1][0], env.boundary[1][1])

        goals.append([x,y])

    return starts,goals

def initialize_libraries(library_fnames=["guided_mrmp/database/2x3_library","guided_mrmp/database/3x3_library","guided_mrmp/database/5x2_library"]):
    """
    Load the 2x3, 3x3, and 2x5 libraries. Store them in self.lib-x- 
    Inputs: 
        library_fnames - the folder containing the library files
    """
    from guided_mrmp.utils.library import Library
    # Create each of the libraries
    print(f"Loading libraries. This usually takes about 10 seconds...")
    lib_2x3 = Library(library_fnames[0])
    lib_2x3.read_library_from_file()
    
    lib_3x3 = Library(library_fnames[1])
    lib_3x3.read_library_from_file()

    
    lib_2x5 = Library(library_fnames[2])
    lib_2x5.read_library_from_file()

    return lib_2x3, lib_3x3, lib_2x5

def plan_decoupled_path(solver_name, start, goal, env, robot_radius,circle_obs, rectangle_obs,vis=False, iter=100, obstacle_tol=0):
    x_bounds = env.boundary[0]
    y_bounds = env.boundary[1]
    space = CircleRectangleObstacleCSpace(x_bounds,y_bounds,robot_radius,obstacle_tol)

    for obs in circle_obs:
        space.addObstacle(Circle(obs[0],obs[1],obs[2]))
    for obs in rectangle_obs:
        space.addObstacle(Rectangle(obs[0],obs[1],obs[2],obs[3]))
    
    program = CSpaceObstacleProgram(space,start,goal,solver_name)
    program.view.w = program.view.h = 640
    program.name = "Motion planning test"

    if vis:
        program.run()
        path = program.planner.getPath()
    else:
        program.planner.planMore(iter)
        path = program.planner.getPath()
        tree = program.planner.getRoadmap()

    return path, tree

def read_agents_from_file(fname):
    pass

def generate_random_agents(num_agents):
    pass

def read_environment_from_file(fname):
    pass