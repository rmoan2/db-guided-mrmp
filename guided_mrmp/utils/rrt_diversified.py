"""
Implements Klampt's various motion planners
"""

from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *

import math
import random
import numpy as np
import itertools
from shapely.geometry import Point, LineString
from shapely.ops import nearest_points

from klampt.plan.cspace import CSpace, MotionPlan
from klampt.vis.glprogram import GLProgram
from klampt.math import vectorops

import matplotlib.pyplot as plt
import matplotlib.patches as patches

class Circle:
    def __init__(self,x=0,y=0,radius=1):
        self.center = (x,y)
        self.radius = radius
        
    def contains(self,point):
        return (vectorops.distance(point,self.center) <= self.radius)

    def drawGL(self,res=0.01):
        numdivs = int(math.ceil(self.radius*math.pi*2/res))
        glBegin(GL_TRIANGLE_FAN)
        glVertex2f(*self.center)
        for i in range(numdivs+1):
            u = float(i)/float(numdivs)*math.pi*2
            glVertex2f(self.center[0]+self.radius*math.cos(u),self.center[1]+self.radius*math.sin(u))
        glEnd()

class Rectangle:
    def __init__(self,x=0,y=0,width=1,height=1):
        self.bottom_left = (x,y)
        self.width = width
        self.height = height
    
    def contains(self,point):
        x,y = point
        x0,y0 = self.bottom_left
        return (x >= x0 and x <= x0+self.width and y >= y0 and y <= y0+self.height)
    
    def drawGL(self):
        x,y = self.bottom_left
        w = self.width
        h = self.height
        glBegin(GL_QUADS)
        glVertex2f(x,y)
        glVertex2f(x+w,y)
        glVertex2f(x+w,y+h)
        glVertex2f(x,y+h)
        glEnd()

class CircleRectangleObstacleCSpace(CSpace):
    def __init__(self, x_bounds=(0.0,1.0), y_bounds=(0.0,1.0), robot_radius=0.05):
        CSpace.__init__(self)
        #set bounds
        # self.bound = [(0.0,1.0),(0.0,1.0)]
        self.bound = [(x_bounds[0], x_bounds[1]), (y_bounds[0], y_bounds[1])]
        #set collision checking resolution
        self.eps = 1e-3
        #setup a robot with radius 0.05
        self.robot = Circle(0,0,robot_radius)
        #set obstacles here
        self.obstacles = []
        #store paths here
        self.paths = []

    def addObstacle(self,obs):
        self.obstacles.append(obs)
    
    def feasible(self,q):
        #bounds test
        if not CSpace.feasible(self,q): return False
        #TODO: Problem 1: implement your feasibility tests here
        #currently, only the center is checked, so the robot collides
        #with boundary and obstacles
        for o in self.obstacles:
            if o.contains(q): return False
        return True

    def drawObstaclesGL(self):
        glColor3f(0.2,0.2,0.2)
        for o in self.obstacles:
            o.drawGL()

    def drawRobotGL(self,q):
        glColor3f(0,0,1)
        newc = vectorops.add(self.robot.center,q)
        c = Circle(newc[0],newc[1],self.robot.radius)
        c.drawGL()

        
    def keyboardfunc(self,key,x,y):
        if key==' ':
            if self.optimizingPlanner or not self.path:
                print("Planning 1...")
                self.planner.planMore(1)
                self.path = self.planner.getPath()
                self.G = self.planner.getRoadmap()
                self.refresh()
        elif key=='p':
            if self.optimizingPlanner or not self.path:
                print("Planning 100...")
                self.planner.planMore(100)
                self.path = self.planner.getPath()
                self.G = self.planner.getRoadmap()
                self.refresh()
       
    def display(self):
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        glOrtho(0,1,1,0,-1,1);
        glMatrixMode(GL_MODELVIEW)
        glLoadIdentity()

        glDisable(GL_LIGHTING)
        self.space.drawObstaclesGL()
        if self.path:
            #draw path
            glColor3f(0,1,0)
            glBegin(GL_LINE_STRIP)
            for q in self.path:
                glVertex2f(q[0],q[1])
            glEnd()
            for q in self.path:
                self.space.drawRobotGL(q)
        else:
            self.space.drawRobotGL(self.start)
            self.space.drawRobotGL(self.goal)

        if self.G:
            #draw graph
            V,E = self.G
            glEnable(GL_BLEND)
            glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA)
            glColor4f(0,0,0,0.5)
            glPointSize(3.0)
            glBegin(GL_POINTS)
            for v in V:
                glVertex2f(v[0],v[1])
            glEnd()
            glColor4f(0.5,0.5,0.5,0.5)
            glBegin(GL_LINES)
            for (i,j) in E:
                glVertex2f(V[i][0],V[i][1])
                glVertex2f(V[j][0],V[j][1])
            glEnd()
            glDisable(GL_BLEND)

class RRTDiversified():
    def __init__(self, robot_starts, robot_goals, bounds, circ_obstacles, rect_obstacles):
        self.robot_starts = robot_starts
        self.robot_goals = robot_goals
        self.bounds = bounds
        self.circ_obstacles = circ_obstacles
        self.rect_obstacles = rect_obstacles
        
        self.cspace = CircleRectangleObstacleCSpace(x_bounds=bounds[0], y_bounds=bounds[1], robot_radius=0.5)
        for (x, y, r) in circ_obstacles:
            self.cspace.addObstacle(Circle(x, y, r))
        for (x, y, w, h) in rect_obstacles:
            self.cspace.addObstacle(Rectangle(x, y, w, h))
            
        self.robot_paths = []
        
        MotionPlan.setOptions(type="rrt*", perturbationRadius=0.25, bidirectional=True)
    
    def run(self):
        for start, goal in zip(self.robot_starts, self.robot_goals):
        
            mp = MotionPlan(self.cspace, type="rrt*", perturbationRadius=0.25, bidirectional=True)
            mp.setEndpoints(start, goal)
            mp.setCostFunction(self.edgeCost)
            
            mp.planMore(1000)
            path = mp.getPath()
            self.robot_paths.append(path)
            
            mp.close()
            
    def pointCostDensity(self, x):
        x = np.array(x)
        cost_density = 0
        for path in self.robot_paths:
            for p1, p2 in itertools.pairwise(path):
                point = Point(x)
                line_segment = LineString([p1, p2])
                distance = point.distance(nearest_points(line_segment, point)[1])
                cost_density += np.exp(-distance)
        return cost_density
    
    def edgeCost(self, x1, x2):  
        x2 = np.array(x2)
        sum_dist = 0
        for path in self.robot_paths:
            for loc in path:
                sum_dist += np.linalg.norm(x2 - np.array(loc))
        cost = np.exp(-sum_dist)
        print(cost)
        return cost   
    
    def plot(self):
        fig, ax = plt.subplots(1)
        
        for (x, y, r) in self.circ_obstacles:
            circle = patches.Circle((x, y), r, facecolor='black', edgecolor=None)
            ax.add_patch(circle)
        for (x, y, w, h) in self.rect_obstacles:
            rect = patches.Rectangle((x, y), w, h, facecolor='black', edgecolor=None)
            ax.add_patch(rect)
        
        # colors = ['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728', '#9467bd', '#8c564b', '#e377c2', '#7f7f7f', '#bcbd22', '#17becf']
        for i, path in enumerate(self.robot_paths):
            path = np.array(path)
            print(f"Path {i}: {path}")
            x_coords = path[:,0]
            y_coords = path[:,1]
            plt.plot(x_coords, y_coords, label=f'Robot {i}')

        plt.legend(loc='upper left')
        ax.set_aspect('equal')

        plt.show()


def main():
    bounds = [(0, 10), (0, 10)]
    robot_starts = [[0.5, 0.5], [9.5, 9.5]]
    robot_goals = [[9.5, 9.5], [0.5, 0.5]]
    circ_obstacles = [(3.5, 3.5, 0.5), (6.5, 6.5, 0.5)]
    rect_obstacles = []
    
    drrt = RRTDiversified(robot_starts, robot_goals, bounds, circ_obstacles, rect_obstacles)
    drrt.run()
    drrt.plot()


if __name__=='__main__':
    import argparse
    
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--seed", 
        type=int, 
        default=None
    )
    args = parser.parse_args()
    
    if args.seed:
        np.random.seed(args.seed)
        random.seed(args.seed)
    
    main()
    
    
        
    