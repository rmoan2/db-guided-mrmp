from setuptools import setup

setup(name='guided_mrmp',
      version='0.1',
      description='Guided Multi-Robot Motion Planning',
      install_requires=['numpy',
                        'matplotlib',
                        'shapely',
                        'pyyaml',
                        'cvxpy',
                        'scipy',
                        'casadi',
                        'klampt',
      ],
)