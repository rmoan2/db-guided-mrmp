import time
import random
import numpy as np
import sys

sys.path.append('../')

from guided_mrmp.utils import Env, Roomba, Robot, create_random_starts_and_goals

from guided_mrmp.simulator import Simulator
from guided_mrmp.utils.helpers import initialize_libraries
from guided_mrmp.controllers import MultiPathTrackerDB

from guided_mrmp.utils.library import Library
from guided_mrmp.main import load_settings, set_python_seed, initialize_robots

class_function_names_dict = {'Roomba': Roomba}


def get_random_key_idx(lib : Library):
    return list(lib.key_to_idx.items())[np.random.choice(lib.get_size())]


def get_robots_from_key(key):
    if len(key) % 4 != 0:
        print(f"Invalid key: {key}")
        return None
    print(key)
    
    robot_starts = []
    robot_goals = []
    for substr in [key[i:i+4] for i in range(len(key) // 4)]:
        start_x = 1.5 * (int(substr[0]) + 1)
        start_y = 1.5 * (int(substr[1]) + 1)
        goal_x = 1.5 * (int(substr[2]) + 1)
        goal_y = 1.5 * (int(substr[3]) + 1)
        
        robot_starts.append([start_x, start_y, 0.0])
        robot_goals.append([goal_x, goal_y, 0.0])
        
    return robot_starts, robot_goals


def get_solution(lib : Library, key, idx):
    num_robots = len(key) // 4
    sol = lib.np_data[num_robots-1][idx]
    return sol


def run(settings_file, env_file, seed, dont_load_libs):

    if seed:
        set_python_seed(seed)
    
    # Load the settings
    settings = load_settings(settings_file)
    environment = load_settings(env_file)

    # Load and create the environment
    circle_obstacles = environment['circle_obstacles']
    rectangle_obstacles = environment['rectangle_obstacles']
    x_range = environment['x_range']
    y_range = environment['y_range']
    env = Env(x_range, y_range, circle_obstacles, rectangle_obstacles)

    # Load the dynamics models
    dynamics_models_st = settings['dynamics_models']
    dynamics_models = []
    for model in dynamics_models_st:
        dynamics_models.append(class_function_names_dict[model](settings))

    # Load the libraries
    if args.dont_load_libs: libs = [None, None, None]
    else: libs = initialize_libraries()
    # lib_2x3 = Library("guided_mrmp/database/2x3_library/")
    # lib_2x3.read_library_from_file()
    
    # Randomly generate the robots
    key, sol_idx = get_random_key_idx(libs[0], 1)
    robot_starts, robot_goals = get_robots_from_key(key)
    robot_radii = settings['robot_radii']
    target_v = settings['target_v']
    robots = initialize_robots(robot_starts, robot_goals, dynamics_models, robot_radii, target_v, env, settings)

    # Create the Guided MRMP policy
    T = settings['prediction_horizon']
    DT = settings['discretization_step']
    policy = MultiPathTrackerDB(env=env,
                                 initial_positions=robot_starts, 
                                 dynamics=dynamics_models[0], # NOTE: Using the same dynamics model for all robots for now
                                 target_v=target_v, 
                                 T=T, 
                                 DT=DT, 
                                 waypoints=[robot.waypoints for robot in robots], 
                                 settings=settings
                                )

    # Create the simulator
    # show_vis = settings['simulator']['show_plots']
    show_vis = settings['simulator']['show_plots']
    sim = Simulator(robots, dynamics_models, env, policy, settings)

    # Run the simulation
    start = time.time()
    sim.run(libs, show_vis)
    end = time.time()
    print(f"Simulation took {end-start} seconds")


if __name__ == "__main__":
    
    lib_2x3 = Library("guided_mrmp/database/2x3_library/")
    lib_2x3.read_library_from_file()
    
    key, idx = get_random_key_idx(lib_2x3)
    print('Key:', key)
    print('Index:', idx)
    
    starts, goals = get_robots_from_key(key)
    print('Starts:', starts)
    print('Goals:', goals)
    
    sol = get_solution(lib_2x3, key, idx)
    print('Solution:', sol)
    
    # import argparse
    
    # parser = argparse.ArgumentParser()
    # parser.add_argument(
    #     "--settings_file", 
    #     type=str, 
    #     default="settings_files/settings.yaml"
    # )
    # parser.add_argument(
    #     "--env_file", 
    #     type=str, 
    #     default="settings_files/env.yaml"
    # )
    # parser.add_argument(
    #     "--seed", 
    #     type=int, 
    #     default=None
    # )
    # parser.add_argument(
    #     "--dont_load_libs", 
    #     action='store_true'
    # )
    # args = parser.parse_args()
    
    
    
    
    